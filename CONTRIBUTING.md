# Contributing

We would love to get merge requests from everyone! But first, you must know that by participating in this project, you agree to abide by the [Contributor Covenant 1.4][conduct] code of conduct.

[conduct]: https://www.contributor-covenant.org/version/1/4/code-of-conduct.txt

First clone the repo and then make a new branch with your user name (or name, whatever suit you better) and when you finish all those amazing changes submit a merge request to the master branch.

And now the ball is on our field. We are not professional programmers (actually, we're chemists) so it may take some time for us to review and check the changes.
If you feel like we are taking a LOT of time (like, weeks) please, don't hesitate to contact any member of the GQC group.

Some things that will increase the chance that your pull request is accepted:

* Write tests.
* Follow [PEP 8 -- Style Guide for Python Code][style].
* Write [good commit messages][commit].

[style]: https://www.python.org/dev/peps/pep-0008/#class-names
[commit]: http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html