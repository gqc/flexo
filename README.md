# Flexo: Conformer generator
## Introduction
Flexo is a conformer generator for organic molecules. It builds the conformers stochastically variating dihedral angles using a genetic algorithm to obtain the lowest energy conformations. It's pretty easy to use and configure (see configs.ini) and to add different methods for energy calculations or optimizations by creating the proper Calculator class inside calculators.py (see the docstring for the "recipe")
## Installing Flexo
### Required Python modules
Please see [requirements.txt] for more information.
* [numpy](https://numpy.org/)

### Required external software
Please see the [notes] in our wiki about installing that program.
* [OpenBabel](http://openbabel.org/wiki/Main_Page) (with its libraries).

  In Debian/Ubuntu you should look for libopenbabel-dev or libopenbabel7.

### Optional external software
* [OpenMOPAC](http://openmopac.net/)
* [DFTB+](https://www.dftbplus.org/)

Install the required packages and then just clone the repository:
```bash
$ git clone https://gitlab.com/gqc/flexo.git
```
## Running Flexo

**Warning**: When you make your .ini files don't forget to turn **OFF** the "Write Unicode BOM" option of your text editor or Flexo is going to crash (Actually, in 95% of the cases you should set it like that. See [UTF and BOM FAQ] for more.)

Let's generate 5 conformers (in .xyz format, one conformer per file) of diphe.mol2 (located inside example_molecules), running the genetic algorithm for 20 generations with a population of 100 members. For calculating the energy we're going to use Molecular Mechanics with MMFF94 as our force field. 
Our configs.ini (I'm going to omit the separators and comments inside it for simplicity's sake but you should take a look to it) should have the following changes:
```ini
[Method]
theory_level = MM

[Molecular Mechanics parameters]
force_field = MMFF94

[Genetic Algorithm parameters]
members_number = 100
max_gen = 20

[Misc. configurations]
max_final_conformers = 5

[Output configurations]
out_format = xyz
multimolecule_file = False
```

Then we run FLEXO: 
```bash 
$ ./flexo.py example_molecules/diphe.mol2 -c configs.ini 
```

When the program finishes, we are going to have a new directory called diphe_mm_conformers that contains 5 .xyz files called diphe_conformerX (X going from 0 to 4) and a log file diphe_report.log with the relevant information of the run.

[UTF and BOM FAQ]: http://www.unicode.org/faq/utf_bom.html
[notes]: https://gitlab.com/gqc/flexo/wikis/Notes-about-installing-openbabel-and-pybel.#how-to-compile-openbabel-to-make-it-work-with-python-35-and-dont-die-in-the-process
[requirements.txt]: https://gitlab.com/gqc/flexo/blob/master/requirements.txt

For more infomation, see the Documentation page https://gqc.quimica.unlp.edu.ar/flexodocs