import sys
import os
import inspect

try:
    from flexo.flexo_main import main
except ModuleNotFoundError:
    print("Flexo package not installed, searching for FLEXO on script's base folder.")
    SCRIPT_DIR = os.path.realpath(os.path.dirname(inspect.getfile(inspect.currentframe())))
    sys.path.append(os.path.dirname(SCRIPT_DIR))
    from flexo.flexo_main import main

if __name__ == "__main__":
    main()
