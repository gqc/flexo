import tkinter
from tkinter import messagebox, filedialog, scrolledtext, simpledialog
from PIL import Image, ImageTk
import os
import sys
import multiprocessing
import threading
import configparser
import flexo
import importlib.resources

def get_logo_path():
    with importlib.resources.path("flexo.gui.assets.images", "flexo-logo.png") as path:
        return str(path)

class AddedMolecule(tkinter.Frame):
    def __init__(self, master, molname):
        super().__init__(master=master)
        self.quitbutton = tkinter.Button(self, text='X')
        self.quitbutton.grid()

        self.molname = tkinter.Text(self, text=molname)
        self.molname.grid(row=0, column=1, sticky=tkinter.E+tkinter.W)

class TkLogger(scrolledtext.ScrolledText):
    """
    File-like class to log into FLEXO window.
    """
    colors_dict = {'91m': 'red',
                   '92m': 'green',
                   '93m': '#FFD500', #Golden yellow
                   '94m': 'blue'}
    def __init__(self, *args, outputfile=None, **kwargs):
        if outputfile is not None:
            self.outputfile = outputfile
        else:
            self.outputfile = None
        super().__init__(*args, **kwargs)
        sys.stdout = self

        for tag, col in self.colors_dict.items():
            self.tag_config(tag, foreground=col)

    def modifytextforfunc(func):
        def inner(self, *args, **kwargs):
            self.configure(state='normal')
            func(self, *args, **kwargs)
            self.configure(state='disabled')
        return inner

    @modifytextforfunc
    def log(self, text):
        self.insert(tkinter.END, text)
        thispos = self.search("\033[", '1.0', stopindex='end')
        c=0
        while thispos:
            textcolor = self.get(thispos+ '+2 chars', thispos + '+5 chars')
            # ~ print(thispos + "\t" + textcolor, file=sys.__stdout__)
            endpos = self.search("\033[0m", thispos, stopindex='end')

            self.delete(endpos, endpos+ '+4 chars')
            self.delete(thispos, thispos+ '+5 chars')

            self.tag_add(textcolor, thispos, endpos)
            thispos = self.search("\033[", thispos, stopindex='end')


    def write(self, text):
        self.log(text)

    @modifytextforfunc
    def clear(self, index1='1.0', index2=tkinter.END):
        self.delete(index1, index2)

    def write_log_to_file(self, filename):
        with open(filename, 'w') as outputfile:
            outputfile.write(self.get(index1='1.0', index2='end'))

    def destroy(self):
        if self.outputfile is not None:
            self.write_log_to_file(self, outputfile)
        super().destroy()

    #For compatibility
    def flush(self):
        pass

class MainWindow(tkinter.Frame):
    def __init__(self, master=None):
        self.molecules = list()
        self.molstrings = tkinter.StringVar()
        self.configwindow = None
        self.configs = None

        super().__init__(master=master)

        myfont = ('Helvetica', 40)

        #### Menu Frame
        self.menuframe = tkinter.Frame(self)
        self.menuframe.grid(column=0, sticky=tkinter.E)

        self.topmenu = tkinter.Menubutton(self.menuframe, text='Help', anchor=tkinter.W)
        self.topmenu.grid(sticky=tkinter.W)
        self.help = tkinter.Menu(self.topmenu)
        self.topmenu['menu'] = self.help
        

        helpmessage = '''Conformer generation tool developed by the Grupo de Química Computacional - CEQUINOR
For more info visit: https://gitlab.com/gqc/flexo'''
        showhelp = lambda : messagebox.showinfo(title='About', message=helpmessage)
        self.help.add_command(label='About', command=showhelp)
        
        
        self.dataframe = tkinter.PanedWindow(self, orient=tkinter.VERTICAL, sashwidth=5, sashrelief=tkinter.RIDGE)
        self.dataframe.grid(column=0, row=1, sticky=tkinter.E + tkinter.W + tkinter.N + tkinter.S)
        
        #### Up Frame
        self.up_frame = tkinter.Frame(self.dataframe, bd=1, relief=tkinter.GROOVE)
        self.dataframe.add(self.up_frame, sticky=tkinter.W+tkinter.E+tkinter.N)
        self.dataframe.paneconfig(self.up_frame, minsize=128) # MAGIC NUMBER, current size of upper left widget

        self.up_left_frame = tkinter.Frame(self.up_frame, bd=1, relief=tkinter.GROOVE)
        self.up_left_frame.grid(row=0)

        #Search for the logo on source dir
        self.logo_image = ImageTk.PhotoImage(file=get_logo_path())
        
        self.logo = tkinter.Label(master=self.up_left_frame, image=self.logo_image)# text='FLEXO', font=myfont)
        self.logo.grid(sticky=tkinter.E+tkinter.W)
        self.runbutton = tkinter.Button(self.up_left_frame, text='Run!', font=myfont+('bold',))
        self.runbutton.grid(row=1, sticky=tkinter.E+tkinter.W)
        self.runbutton['command'] = self.runflexo
        
        
        self.up_right_frame = tkinter.Frame(self.up_frame, bd=1, relief=tkinter.GROOVE)
        self.up_right_frame.grid(column=1, row=0, sticky=tkinter.S+tkinter.E+tkinter.W+tkinter.N)

        self.buttonframe = tkinter.Frame(self.up_right_frame)
        self.buttonframe.grid(row=1, sticky=tkinter.E+tkinter.W)
        
        actionbuttons = ['Add', 'Config', 'Clear']
        self.controlbuttons=dict()
        for i, buttonname in enumerate(actionbuttons):
            self.controlbuttons[buttonname] = tkinter.Button(self.buttonframe, text=buttonname)
            self.controlbuttons[buttonname].grid(column=i, row=0)#, row=1)#, sticky=tkinter.S)
        
        self.controlbuttons['Add']['command'] = self.askformols
        self.controlbuttons['Config']['command'] = self.getconfigs 
        self.controlbuttons['Clear']['command'] = self.clearmols
        
        self.mollist = tkinter.Listbox(self.up_right_frame, listvariable=self.molstrings)
        self.mollist.grid(row=0, columnspan=len(actionbuttons), sticky=tkinter.E+tkinter.W+tkinter.S+tkinter.N)        

        self.up_right_frame.columnconfigure(0, weight=1)                  
        self.up_frame.columnconfigure(1, weight=1, minsize=190)
        
        #### Down Frame
        self.down_frame = tkinter.LabelFrame(self.dataframe, bd=1, relief=tkinter.GROOVE, text='Status')
        self.dataframe.add(self.down_frame)
##        self.down_frame.columnconfigure(0, weight=1)

        self.logger = TkLogger(self.down_frame, state=tkinter.DISABLED)
        self.logger.grid(sticky=tkinter.N + tkinter.S + tkinter.E + tkinter.W)


        self.statusbar = tkinter.Frame(self.down_frame, bd=1, relief=tkinter.GROOVE)
        self.statusbar.grid(row=1, sticky=tkinter.W+tkinter.E)

        #### Control logger
        self.clearlogger = tkinter.Button(master=self.statusbar, text="Clear Log", command=self.logger.clear, anchor=tkinter.W)
        self.clearlogger.grid(column=0, row=0, sticky=tkinter.W)
        self.writelogger = tkinter.Button(master=self.statusbar, text="Save Log", command=self.write_log_to_file, anchor=tkinter.W)
        self.writelogger.grid(column=1, row=0, sticky=tkinter.W)

        self.statusdialog = tkinter.Label(self.statusbar, text='Setting up...', anchor=tkinter.E)
        self.statusdialog.grid(column=2, row=0, sticky=tkinter.W+tkinter.E)

        self.columnconfigure(0, weight=1)
        self.rowconfigure(1, weight=1)
        self.dataframe.rowconfigure(0, weight=0)
        self.dataframe.rowconfigure(1, weight=1)
        self.up_frame.rowconfigure(0, weight=1)
        self.up_right_frame.rowconfigure(0, weight=1)
        self.down_frame.rowconfigure(0, weight=1)
        self.down_frame.columnconfigure(0, weight=1)
        self.statusbar.columnconfigure(2, weight=1)

        # ~ self.runflexothread = None
        self.runflexoqueue = multiprocessing.Queue()

    def askformols(self):
        newmols = filedialog.askopenfilenames(parent=self, title='Open mols...', multiple=True,
                                              filetypes=(('Sybyl mol2', '*.mol2'), ('XYZ File', '*.xyz'), ('All Files', '*')))
        self.molecules += list(newmols)
        self.molstrings.set(" ".join(self.molecules))

    def clearmols(self):
        self.molecules = list()
        self.molstrings.set("")# = tkinter.StringVar()

    def getconfigs(self):
        self.configwindow = ConfigWindow(self)
        for configtype in self.configoptions:
            self.configwindow.add_config_widget(configtype, self.configoptions[configtype])

    def runflexo(self):
        if self.configwindow is not None:
            self.configs = self.configwindow.configs
        if self.configs is None:
            if os.path.exists('configs.ini'):
                self.configs = flexo.MyConfigReader()
                self.configs.read_configs('configs.ini')

        self.statusdialog.configure(text="Running!")
        for molfile in self.molecules:
            self.runflexoqueue.put(molfile)

        self.launchflexo()
        # ~ if self.runflexothread is None:
            # ~ self.flexothrd = threading.Thread(target=self.launchflexo)
            # ~ self.flexothrd.start()
        # ~ else:
            # ~ self.flexothrd.run()
        # ~ self.flexothrd.join()
        self.statusdialog.configure(text="Finished")

    def launchflexo(self):
        """Launches the FLEXO runs so the GUI wont block"""
        while not self.runflexoqueue.empty():
            molfile = self.runflexoqueue.get_nowait()
            print(f"Running {molfile}")
            try:
                flexo.main(configs=self.configs, molecule_file=molfile)
            except SystemExit:
                print(f"Early termination on file {molfile}")
                pass
        # ~ self.flexothrd.join() 
        # ~ self.flexothrd.close() 

    def write_log_to_file(self):
        outlogfile = filedialog.asksaveasfilename(parent=self, title='Write log to...',
                                              filetypes=(('Log file', '*.log'), ('TXT file', '*.txt'), ('All Files', '*')))
        if outlogfile:
            print(outlogfile, file=sys.__stdout__)
            self.logger.write_log_to_file(outlogfile)

class ConfigWindow(tkinter.Toplevel):
##    # Singleton pattern implementation
##    _instance = None
##
##    def __new__(cls, *args, **kwargs):
##        if cls._instance is None:
##            cls._instance = super(ConfigWindow, cls).__new__(cls)#, *args, **kwargs)
####            cls._instance = tkinter.Toplevel.__new__(cls)
####            cls._instance.__init__(*args, **kwargs)
##        return cls._instance

    def __init__(self, parent, title = None):
        super().__init__(parent)
##        tkinter.Toplevel.__init__(parent)
        if title:
            self.title(title)
        self.parent = parent
##        self.flexo_configs=dict()
        self.configs = None
        self.mainframe = tkinter.Frame(self)
        self.mainframe.grid(sticky=tkinter.N + tkinter.S+tkinter.W + tkinter.E)

        self.configs_body = tkinter.Frame(self.mainframe, bd=1, relief=tkinter.GROOVE)
        self.configs_body.grid(row=0, column=0, sticky=tkinter.N + tkinter.S+tkinter.W + tkinter.E)
        

        self.mainslider = tkinter.PanedWindow(self.configs_body, orient=tkinter.HORIZONTAL, showhandle=True)
        self.columnbuttons=[]
        for i, column in enumerate(['Option', 'Value', 'Description']):
            thiscolumn = tkinter.Button(self.mainslider, text=column, bd=1, relief=tkinter.GROOVE)
            self.columnbuttons.append(thiscolumn)
            thiscolumn.bind("<Configure>", self.update_options_width)
            self.mainslider.add(thiscolumn)
        self.mainslider.grid(row=0, column=0, sticky=tkinter.E + tkinter.W)

        ## Get aproximately the button's text width, to use as the minimum width for that column
        textwidth = lambda button: len(button.cget('text'))*10

        for column_index, column in enumerate(self.mainslider.panes()):
            self.mainslider.paneconfig(column, minsize=textwidth(self.columnbuttons[column_index]))

            
        self.optionscontainer = ScrollCanvas(self.configs_body)
        self.optionscontainer.grid(row=1, column=0, sticky=tkinter.S + tkinter.N + tkinter.E + tkinter.W)
        self.optionscontainer.config(relief=tkinter.GROOVE)
        self.optionscontainer.config(bd=1)
        self.optionscontainer.columnconfigure(0, weight=1)
        self.optionscontainer.rowconfigure(0, weight=1)
        self.configs_body.rowconfigure(1, weight=1)
        self.configs_body.columnconfigure(0, weight=1)

        self.options = []

        self.buttonframe = tkinter.Frame(self.mainframe, bd=1, relief=tkinter.GROOVE)
        self.buttonframe.grid(row=1, column=0, sticky=tkinter.E)#+tkinter.E+tkinter.W)
        actionbuttons = ['Load', 'Save', 'Close']
        self.controlbuttons=dict()
        for i, buttonname in enumerate(actionbuttons):
            self.controlbuttons[buttonname] = tkinter.Button(self.buttonframe, text=buttonname)
            self.controlbuttons[buttonname].grid(column=i, row=0)
        self.controlbuttons['Close']['command'] = self.destroy
        self.controlbuttons['Load']['command'] = self.load_options

        self.mainframe.rowconfigure(0, weight=1)
        self.mainframe.columnconfigure(0, weight=1)

        self.rowconfigure(0, weight=1)
        self.columnconfigure(0, weight=1)
        self.config(relief=tkinter.GROOVE)
        self.config(bd=1)

##        self.wait_window(self)
##        self.attributes('-topmost', 'true')
        self.grab_set()

    def getconfigs(self):
        return self.configs

    def add_config_widget(self, name, optionsdict):
        '''
        optionsdict: dict(optionname: dict(options: options, optiontype: optiontype, description: description))
        '''
        thisoptions = tkinter.LabelFrame(self.optionscontainer.innerframe, bd=1, relief=tkinter.SUNKEN, text=name)
##        thisoptions.pack(fill=tkinter.BOTH, expand=True)
        thisoptions.grid(column=0, sticky=tkinter.E + tkinter.W + tkinter.N+tkinter.S)
        thisoptions.columnconfigure(0, weight=1)

##        self.options.append(thisoptions)
        
        for i, option in enumerate(optionsdict):
            optionframe = tkinter.PanedWindow(thisoptions, bd=1, relief=tkinter.GROOVE)
            optionframe.grid(row=i, column=0, sticky=tkinter.E+tkinter.W)
            
            optionname = tkinter.Label(optionframe, text=option, anchor=tkinter.W)
##            optionname.grid(column=0, row=i)
            if optionsdict[option]['optiontype'] == 'text':
                optionvalue = tkinter.Entry(optionframe)
            elif optionsdict[option]['optiontype'] == 'multiple':
                optionname.varvalue = tkinter.StringVar(optionframe)
                optionvalue = tkinter.OptionMenu(optionframe, optionname.varvalue, *optionsdict[option]['options'])
            elif optionsdict[option]['optiontype'] == 'bool':
                optionname.varvalue = tkinter.IntVar(optionframe)
                optionvalue = tkinter.Checkbutton(optionframe, variable=optionname.varvalue)
##            optionvalue.grid(column=1, row=i)

            optiondescription = tkinter.Label(optionframe, text=optionsdict[option]['description'], anchor=tkinter.NW, wraplength=243)
##            optiondescription.grid(column=2, row=i)

            optionframe.add(optionname)
            optionframe.add(optionvalue)
            optionframe.add(optiondescription)
            self.options.append(optionframe)


##        self.options.append([optionname, optionvalue, optiondescription])
##        totalheight = sum([ctest.optionscontainer.innerframe.children[a].winfo_height() for a in ctest.optionscontainer.innerframe.children])
##        self.optionscontainer.configure(scrollregion=(0, 0, 0, 0))

    def update_options_width(self, *args, **kwargs):
        columnwidths = []
        for column in self.columnbuttons:
            columnwidths.append(column.winfo_width())
        for option in self.options:
            for option_index, optioncolumn in enumerate(option.panes()):
                option.paneconfig(optioncolumn, width=columnwidths[option_index])
            ## Rest 10 from the column width because of the scrollbar
            option.children[option.panes()[-1].string.split(".")[-1]].configure(wraplength=columnwidths[-1]-10)

##            option[column_index].config(width=thiswidth)
##                print(f"child width: {option[column_index].winfo_width()}")

    def load_options(self):
        optionsfile = filedialog.askopenfilenames(parent=self, title='Open config...', filetypes=(('Config ini', '*.ini'), ('All Files', '*')))
        self.configs = flexo.MyConfigReader()
        self.configs.set_configs(config_file=optionsfile)

    def destroy(self):
        self.parent.configs = self.configs
####        self.grab_release()
        super().destroy()


class ScrollCanvas(tkinter.Canvas):
    def __init__(self, master, **kw):
        self.frame = tkinter.Frame(master, bd=1, relief=tkinter.GROOVE)
        
        self.scrollbar = tkinter.Scrollbar(self.frame)
        self.scrollbar.grid(column=1, row=0, sticky=tkinter.N + tkinter.S)
        
        super().__init__(self.frame, **kw)
        self.grid(column=0, row=0, sticky=tkinter.N + tkinter.S + tkinter.E + tkinter.W)
        self.scrollbar['command'] = self.yview

        # Copy geometry methods of self.frame without overriding Text
        # methods -- hack!
        text_meths = vars(tkinter.Text).keys()
        methods = vars(tkinter.Pack).keys() | vars(tkinter.Grid).keys() | vars(tkinter.Place).keys()
        methods = methods.difference(text_meths)

        for m in methods:
            if m[0] != '_' and m != 'config' and m != 'configure':
                setattr(self, m, getattr(self.frame, m))

        self.rowconfigure(0, weight=1)
        self.innerframe = tkinter.Frame(master=self)
        self.innerframe.grid(sticky=tkinter.N+tkinter.S+tkinter.E+tkinter.W)
        self.innerframe.columnconfigure(0, weight=1)

        self.bind("<Configure>", self.resize_frame)
        self.innerframe.bind("<Configure>", lambda e: self.configure(scrollregion=self.innerframe.bbox("all")))

        self.inf = self.create_window((0, 0), window=self.innerframe, anchor=tkinter.NW)
        self.configure(yscrollcommand=self.scrollbar.set)

    def innerframe(self, *args, **kwargs):
        pass
    def resize_frame(self, e):
        self.itemconfig(self.inf, height=e.height, width=e.width)
        
class ConfigOptions:
    """
    Class used to add options to the configure dialog.
    """
    def __init__(self, optionsdict=None, logger=None):
        if optionsdict is not None:
            if type(optionsdict) == dict:
                self.optionsdict == optionsdict
            else:
                raise TypeError("optionsdict should be of dict type")
        else:
            self.optionsdict = dict()

        if os.path.isfile("./configs.ini"):
            if logger:
                logger.log("configs.ini found, loading that as default.\n")
            

    def add_config(self, optionclass, optionname, optiontype, description, options=None):
##        thisoption=dict()
        thisoption = {'optiontype': optiontype, 'description': description,
                                      'options': options}

        if optionclass in self.optionsdict:
            self.optionsdict[optionclass][optionname] = thisoption
        else:
            self.optionsdict[optionclass] = {optionname: thisoption}



def main():
    root = tkinter.Tk()
    root.title("FLEXO")

    flexogui = MainWindow(root)

    flexogui.grid(sticky=tkinter.N+tkinter.S+tkinter.E+tkinter.W)
    root.columnconfigure(0, weight=1)
    root.rowconfigure(0, weight=1)

    ##testoptions = {'opt1': {'options': ('1', '2', '3'), 'optiontype': 'multiple', 'description': 'First test option'},
    ##               'opt2': {'optiontype': 'text', 'description': 'Second test option'}}

    myconfigoptions = ConfigOptions(logger=flexogui.logger)
    myconfigoptions.add_config("Method", "Theory Level", "multiple", "Semi-Enpirical or Molecular Mechanics", options=["MM", "SE"])
    myconfigoptions.add_config("Method", "Explorer", "multiple", "Genetic Algorithm or Brute Force", options=["GA", "BF"])

    configoptions = dict()
    configoptions["Method"] = {'Theory level': {'options': ('SE', 'GA'), 'optiontype': 'multiple', 'description': "Theory level to use when evaluating conformers' energy."},
                            'Explorer': {'options': ('GA', 'BF', 'RBF', 'PSO'), 'optiontype': 'multiple', 'description': "Exploration method to search the conformational space."}}

    configoptions["MM"] = {'Force field': {'options': ('SE', 'GA'), 'optiontype': 'multiple', 'description': "Molecular Mechanics force field used to evaluate the conformers' energy."}}

    configoptions["Misc."] = {'Mask': {'optiontype': 'text', 'description': "Theory level to use when evaluating conformers' energy."},
                            'Final optimization': {'optiontype': 'bool', 'description': "Exploration method to search the conformational space."}}
    flexogui.configoptions = configoptions
    # ~ print(flexo.MyConfigReader().DEFAULT_DICT)
    # ~ print(configoptions)
    ##flexogui.configoptions = myconfigoptions.optionsdict

    ##ctest = ConfigWindow(root, 'config')
    ##for configtype in configoptions:
    ##    ctest.add_config_widget(configtype, configoptions[configtype])

    ##root.rowconfigure(0, weight=1)
    ##a = ScrollCanvas(root, bd=12, relief=tkinter.GROOVE)
    ##a.grid(sticky=tkinter.N+tkinter.S)
    root.mainloop()
