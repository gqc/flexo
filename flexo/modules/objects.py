from math import radians
import re

import numpy as np
try:
    from flexo.modules.cfunctions.cfunctions import modify_dihedral
except ImportError:
    print("No pude cargar flexo_cfunctions en objects.py")
    from flexo.modules.cfunctions.cfunctions import modify_dihedral
from flexo.modules.mainfunctions import calculate_rmsd, find_connected
from scipy.sparse.csgraph import shortest_path

class MyConformer:
    """
    MyConformer class is used to hold conformer object data.

    Attributes
    ----------
    atoms_coords : np.array
        Array containing the cartesian coordinates of the conformer.
    paramlst : list(Parameter)
        List of Parameter objects which are going to be used by the genetic algorithm.
    energy :  float
        Calculated energy of the conformer.
    fitness : float
        Fitness value granted by the genetic algorithm fitness function.
    """

    def __init__(self, atoms_coords, paramlst=None, energy=None):
        """
        The constructor for MyConformer class.

        Parameters
        ----------
        atoms_coords : np.array
            Array containing the cartesian coordinates of the conformer.
        paramlst : list(Parameter)
            List of Parameter objects which are going to be used by the genetic algorithm.
        energy : float
            Calculated energy of the conformer.
        """
        self.atoms_coords = atoms_coords
        self.energy = energy
        if paramlst is None:
            paramlst = tuple()
        self.paramlst = tuple(paramlst)
        self.fitness = None

    def __hash__(self):
        return self.paramlst.__hash__()

    def __str__(self):
        return "".join([f"{param.value: >7.2f} " for param in self.paramlst]) + str(self.energy)

    def write_to_file(self, outfile, ID=0):
        """
        Write the conformer to an output file with an optional index to classify it.
        The line written is a list of the conformers' dihedrals, the conformer's energy and the index at the end

        Parameters
        ----------
        outfile : file
            File to write conformer to
        index : int
            Optional classifier, for example, particle ID in PSO.
        """
        outfile.write(str(self) + f" {ID}\n")

class MyMolecule:
    """
    MyMolecule class is used to hold molecule object data.

    Attributes
    ----------
    file_path : str 
        The path pointing to the .mol2 file holding the molecule.
    name : str
        The name of the molecule.
    initial_paramlst : list(Parameter)
        List of initial Parameter objects.
    initial_conformer : MyConformer
        Initial conformer of the molecule.
    bondlst : list, int, int, int)
        List of bonds between atom1, atom2 and bond type, modified by us. See function.
    conformerlst : list
        Confomers generated for the molecule.
    rotatable_bondslst : list
        Rotatable bonds of the molecule.
    connection_matrix : list
        Connection matrix of the molecule.
    atoms_properties : (dic, list(int), list(str), list(str), list(float))
        unique id, symbol, atom type and charge of each atom.
    _initial_coords : np.array(float)
        x, y, z coords of each atom in the initial confomer (the one in the .mol2 file).
    original_bondlst : list, int, int, int or str)
        List of bonds between atom1, atom2 and bond type in the .mol2 file.

    Methods:
        _read_mol2(self, file_path)
        params_to_conformer(self, paramlst)
        calculate_connection_matrix(self)
    """

    def __init__(self, file_path=None, paramlst=None):
        """
        The constructor for MyMolecule class.

        Parameters
        ----------
        file_path :str 
            The name of the .mol2 file holding the molecule.
        paramlst : list(Parameter)
            List of Parameter objects which are going to be used by the genetic algorithm.
        """
        self.file_path = file_path
        self.name = file_path.split("/")[-1].rsplit(".", maxsplit=1)[0]
        self.initial_paramlst = paramlst
        self.atoms_properties = {}
        self._initial_coords = []
        self.bondlst = []
        self.conformerlst = []
        self.rotatable_bondslst = []
        self.bond_in_ringdic = {}
        self.original_bondlst = []

        self._load_mol2()
        self.initial_conformer = MyConformer(self._initial_coords)
        self.connection_matrix = self._calculate_connection_matrix()
        self.bond_in_ringdic = self._bond_is_in_ring(self.connection_matrix)
        self.distance_filter=self.create_distance_filter(radii_cut={},scale_factor=0.75)

    def _load_mol2(self):
        """
        Read atoms coordinates, properties and bonds contained in a .mol2 file.

        Returns
        -------
        _initial_coords : np.array, float
            x, y, z coords of each atom.
        atoms_properties : (list, int, str, str, float)
            unique id, symbol, atom type and charge of each atom.
        bondlst : (list, int, int, int)
            List of bonds between atom1, atom2 and bond type.
        original_bondlst : (list, int, int, int or str)
            Lis of bonds between atom1, atom2 and bond type in the .mol2 file.
        """
        #These regexs ONLY catch what is BETWEEN the delimiter but NOT the delimiter!
        MOLECULE_RTI_RE = re.compile(r'(?s)(?<=@<TRIPOS>MOLECULE\n).*(?=@<TRIPOS>ATOM)')
        COORDS_AND_PROP_RE = re.compile(r'(?s)(?<=@<TRIPOS>ATOM\n).*(?=@<TRIPOS>BOND)')
        BONDS_RE = re.compile(r'(?s)(?<=@<TRIPOS>BOND\n).*(?=^@|)')

        __atoms_coords = []
        __atoms_properties = {}
        __bondlst = []
        original_bondlst = []
        with open(self.file_path, "r") as molecule_file:
            molecule_file_read = molecule_file.read()

            molecule_rti_found = re.search(MOLECULE_RTI_RE, molecule_file_read)
            coord_props_found = re.search(COORDS_AND_PROP_RE, molecule_file_read)
            bonds_found = re.search(BONDS_RE, molecule_file_read)

            unique_id_lst = []
            symbol_lst = []
            atom_type_lst = []
            charge_lst =[]

            for line in coord_props_found.group(0).split("\n"):
                sp_line = line.split()
                try:
                    #Coordinates loading
                    x = float(sp_line[2])
                    y = float(sp_line[3])
                    z = float(sp_line[4])
                    __atoms_coords.append([x, y, z])

                    #Properties loading
                    unique_id_lst.append(int(sp_line[0]))
                    symbol_lst.append(str(sp_line[5].split(".")[0]))
                    atom_type_lst.append(str(sp_line[5]))
                    charge_lst.append(float(sp_line[8]))

                except IndexError:
                    break

            __atoms_properties["unique_id"] = unique_id_lst
            __atoms_properties["symbol"] = symbol_lst
            __atoms_properties["atom_type"] = atom_type_lst
            __atoms_properties["charge"] = charge_lst

            for line in bonds_found.group(0).split("\n"):
                sp_line = line.split()
                try:
                    #Bonds loading
                    atom1 = int(sp_line[1])
                    atom2 = int(sp_line[2])
                    original_bondlst.append([atom1, atom2, sp_line[3]])
                    if sp_line[3].isdigit():
                        bond_order = int(sp_line[3])
                    else:
                        bond_order = 10 #Arbitrarly picked to be used in
                                        #calculate_connection_matrix().
                    __bondlst.append([atom1, atom2, bond_order])
                except IndexError:
                    break

        self._initial_coords = np.array(__atoms_coords)
        self.atoms_properties = __atoms_properties
        self.bondlst = __bondlst
        self.original_bondlst = original_bondlst
        self.molecule_rti = molecule_rti_found.group(0)
        return self._initial_coords, self.atoms_properties, self.bondlst, self.original_bondlst, self.molecule_rti


    def _bond_is_in_ring(self, con_mtx, start=0):
        """
            Set a dic ((int, int):bool) with atom1, atom2 unique_id as key and a boolean as value, depending
            if the bond between those atoms is or not in a ring. The key tuple is sorted sorted.
        """
        cycles=list()
        bonds=set()
        ## Depth First Search algorithm in recursive version to find te cycles
        ## Searchs a path, backs to last branching and checks if the node was visited before
        ## The cycles found includes the border of fused cycles, but for this is irrelevant
        def dfsearch(row, con_mtx, searched=list(), last=None):
            if last is None:
                last=row
            searched.append(row)
            for column in range(np.shape(con_mtx)[0]):
                if con_mtx[row][column] and column != last and column != row:
                    bonds.add(frozenset((row+1, column+1)))
                    if column not in searched:
                        dfsearch(column, con_mtx, searched, row)
                    else:
                        cycle=searched[searched.index(column):]
                        if cycle not in cycles:
                            cycles.append(cycle)
            searched.remove(row)
        dfsearch(start, con_mtx)
        ## Pass adjacency matrix to atom numbers
        cycles = [[index + 1 for index in cycle] for cycle in cycles]
        ## Create the same dict that the openbabel function returned, for compatibility
        ## TODO: enhance the dict using only the values where it is true
        isinringdict=dict()
        for bond in bonds:
            isinringdict[tuple(sorted(bond))] = False
        for cycle in cycles:
            for i in range(len(cycle)-1):
                isinringdict[tuple(sorted((cycle[i], cycle[i+1])))] = True
            isinringdict[tuple(sorted((cycle[-1], cycle[0])))] = True
        return isinringdict

    def _calculate_connection_matrix(self):
        """
        Function to calculate the connection matrix of the molecule.

        Returns
        -------
        matrix : list
            A list that holds the connection matrix. Equivalent to a NxN matrix where the element a_ij
            is 0 if atoms i and j are not connected, or the bond order else.
        """
        matrixsize = len(self.atoms_properties["symbol"])
        matrix = [0] * matrixsize
        for i in range(matrixsize):
            matrix[i] = [0] * matrixsize
        # bondlst connection: [0]:atom1, [1]:atom2, [2]:bond order
        for (index_atom1, index_atom2, bond_order) in self.bondlst:
            # fill matrix elements atom M,atom N and atomN,atomM with bond order
            matrix[index_atom1 - 1][index_atom2 - 1] = bond_order
            matrix[index_atom2 - 1][index_atom1 - 1] = bond_order

        return matrix


    def detect_fragments(self, d_index):
        r"""
        Return the masks necessary to transform molecule by a dihedral.
        When a rotation is made, we need to define the atoms on each side of the bond, so the
        corresponding atoms are transformed. The bigger fragment is kept fixed and the other is
        rotated by the bond. To indicate each fragment, atoms belonging to each one are marked as 1
        and if not is 0.

        Parameters
        ----------
        d_index : list
            A list containing the ordered indexes of atoms forming the dihedal of interest.

        Returns
        -------
        mask1 : list
            n*3 list corresponding to each coordinate of each atom for the fixed fragment of the
            rotation. a 1 indicates that the coordinate belongs to this fragment, and 0 that it
            doesn't.
        mask2 : list
            n*3 list corresponding to each coordinate of each atom for the fixed fragment of the
            rotation. a 1 indicates that the coordinate belongs to this fragment, and 0 that it
            doesn't.

        Examples
        --------
        For example, with a dihedral ABCD, where the bond B-C is of interest and with atom indexes
        abcd::

            A      D
             \    /
              B--C

        d_index = [a, b, c, d]

        """
        mat_copy = np.copy(self.connection_matrix)

        mat_copy[d_index[1]-1,d_index[2]-1]=0
        mat_copy[d_index[2]-1,d_index[1]-1]=0
        n_components, labels = find_connected(mat_copy)

        mask_shape=(mat_copy.shape[0],3)

        mask1=np.ones(mask_shape)
        mask2=np.zeros(mask_shape)


        # Find the fragment with the most atoms and use that as the first fragment - the one that
        # won't move when the dihedral is rotated.
        if n_components > 1:
            if len(np.where(labels==0)[0]) >= len(np.where(labels==1)[0]):
                fragment_1= np.where(labels==0)[0]
                fragment_2= np.where(labels==1)[0]
            else:
                fragment_1= np.where(labels==1)[0]
                fragment_2= np.where(labels==0)[0]

            for j in range(mask_shape[0]):
                if j  in fragment_1:
                    mask1[j,:]=0*mask1[j,:]
                    mask2[j,:]=1+mask2[j,:]
        else:
            print("Bond "+str(d_index[1])+"-"+str(d_index[2])+"is part of a ring")
        return [mask1.flatten(), mask2.flatten()]


    def modify_dihedral(self, coords, parameter, value):
        """
        Rotate a conformer around a given bond, the central bond of a dihedral.

        Parameters
        ----------
        coords : np.array
            NX3 or N*3 array of the initial atomic coordinates.
        parameter : Parameter
            Parameter object corresponding to the dihedral of interest.
        value : float
            Angle to rotate, in degrees.

        Returns
        -------
        finalcoords : np.array
            Nx3 array with the modified atomic coordinates.

        """
        dihedral_indexs=parameter.atoms
        frags=parameter.masks
        theta=radians(value)
        #dihedral_indexs=np.array(dihedral_indexs)-1

        ##########

        id1=dihedral_indexs[0]-1
        id2=dihedral_indexs[1]-1
        id3=dihedral_indexs[2]-1
        id4=dihedral_indexs[3]-1
        self.initial_paramlst


        mask1=frags[0]
        mask2=frags[1]
        natoms=len(coords)
        finalcoords=np.empty((natoms*3))
        coords=coords.ravel()

        modify_dihedral(coords,finalcoords,mask1,mask2,id1,id2,id3,id4,theta)
        finalcoords=finalcoords.reshape((natoms,3))
        return finalcoords


    def create_new_conformer(self, paramlst, energy=None):
        """
        Create and return a new conformer with the given Parameters.

        Parameters
        ----------
        paramlst : list(Parameter)
            List of parameters that define the new conformer's coordinates.
        energy : float
            Energy of the generated conformer.

        Returns
        -------
        new_conformer : MyConformer
            Conformer created with the given parameters.
        """

        new_coords=self.initial_conformer.atoms_coords
        for final_param, initial_param in zip( paramlst,self.initial_paramlst):
            if "D" in final_param.paramtype:
                value=final_param - initial_param
                new_coords=self.modify_dihedral(new_coords,final_param,value)
        new_conformer = MyConformer(new_coords,paramlst)
        if energy is not None:
            new_conformer.energy = energy
        return new_conformer
    def create_distance_filter(self,radii_cut={},scale_factor=1.):
        rcut={'H': 1.2, 'He': 1.4, 'Li': 1.82, 'Be': 1.53, 'B': 1.92, 'C': 1.7, 'N': 1.55, 'O': 1.52, 'F': 1.47, 'Ne': 1.54, 'Na': 2.27, 'Mg': 1.73, 'Al': 1.84, 'Si': 2.1, 'P': 1.8, 'S': 1.8, 'Cl': 1.75, 'Ar': 1.88, 'K': 2.75, 'Ca': 2.31, 'Ni': 1.63, 'Cu': 1.4, 'Zn': 1.39, 'Ga': 1.87, 'Ge': 2.11, 'As': 1.85, 'Se': 1.9, 'Br': 1.85, 'Kr': 2.02, 'Rb': 3.03, 'Sr': 2.49, 'Pd': 1.63, 'Ag': 1.72, 'Cd': 1.58, 'In': 1.93, 'Sn': 2.17, 'Sb': 2.06, 'Te': 2.06, 'I': 1.98, 'Xe': 2.16, 'Cs': 3.43, 'Ba': 2.49, 'Pt': 1.75, 'Au': 1.66, 'Hg': 1.55, 'Tl': 1.96, 'Pb': 2.02, 'Bi': 2.07, 'Po': 1.97, 'At': 2.02, 'Rn': 2.2, 'Fr': 3.48, 'Ra': 2.83, 'U': 1.86} # vand der Wals  radii from ase.data
        for k,v in radii_cut.items():
            rcut[k]=v

        mc=np.array(self.connection_matrix) #construye matriz de conectividad
        atom_types=self.atoms_properties['symbol'] #lista con los simbolos de los

        c_filter=shortest_path(mc, method='auto', directed=False, return_predecessors=False, unweighted=True)

        c_filter= (c_filter > 3) #crea matriz para filtrar, 1. si estan a mas de 3 enlaces de distancia, 0. en caso contrario
        distance_filter=np.zeros(c_filter.shape)

        for i,symb_i in enumerate(atom_types):
            d1=rcut[symb_i]
            for j,symb_j in enumerate(atom_types):
                d2=rcut[symb_j]

                #multiplica c_filter por la suma de radios de corte y el factor de escala
                distance_filter[i,j]=c_filter[i,j]*(d1+d2)*scale_factor


        #return distance_filter
        return distance_filter


class Parameter(float):
    """
    Parameter class is used to hold the molecular parameters that are going to be modified during the conformational search.
    Inherits from float, and can be trated as a float with special attributes. It is inmutable.

    Parameters
    ----------
    paramtype : str
        Type of the parameter to be used by the genetic algorithm. Dihedral ("D"), Angle ("A"), Bond ("B"). 09/09/19 Right now only D is implemented.
    atoms : list(int)
        Numbers (unique_id like) of the atoms that are going to be implicated in the atribute of the molecule (09/09/19 Right now only a dihedral angle is implemented) to be used by the genetic algorithm.
    value : float
        Value of the parameter obtained from the .mol2 file.
    min_value : float
        Minimum value that the parameter can take during the conformational search.
    max_value : float
        Maximun value that the parameter can take during conformational search.
    masks : str
        Masks to be used.
    """

    def __new__(self, paramtype, atoms, value, min_value, max_value, masks=None):
        return float.__new__(self, value)

    def __init__(self, paramtype, atoms, value, min_value, max_value, masks=None):
        """
        Constructor for Parameter class.

        Parameters
        ----------
        paramtype : str
            Type of the parameter to be used by the genetic algorithm. Dihedral ("D"), Angle ("A"), Bond ("B"). 09/09/19 Right now only D is implemented.
        atoms : list(int)
            Numbers (unique_id like) of the atoms that are going to be implicated in the atribute of the molecule (09/09/19 Right now only a dihedral angle is implemented) to be used by the genetic algorithm.
        value : float
            Value of the parameter obtained from the .mol2 file.
        min_value : float
            Minimum value that the parameter can take during the conformational search.
        max_value : float
            Maximun value that the parameter can take during conformational search.
        masks : str
            Masks to be used.
        """
        float.__init__(value)
        self.paramtype = paramtype
        self.atoms = atoms
        self.value = value
        self.min_value = min_value
        self.max_value = max_value
        self.masks = masks

    def __copy__(self):
        """
        Returns a new Parameter with identical properties.
        """
        return Parameter(self.paramtype, self.atoms, self.value, self.min_value, self.max_value, self.masks)

    #TODO: ver esto
    def newparam(self, value=None):
        """
        Returns a new Parameter with the specified value and the other parameters derived from the existing one.
        If value is None, returns a copy of the existing Parameter.
        """
        if value is None:
            value = self.value
        return Parameter(self.paramtype, self.atoms, value, self.min_value, self.max_value, self.masks)

    def __deepcopy__(self, memo):
        return Parameter(self.paramtype, self.atoms, self.value, self.min_value, self.max_value, self.masks)

    def __repr__(self):
        return self.value.__repr__()

    def __str__(self):
        return self.value.__str__()
