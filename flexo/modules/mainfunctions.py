"""This module contains utility functions that FLEXO uses.
Functions to run energy/optimization calculations, to process information from the molecular file, to
interpretate bonds or atoms, etc.

The most useful in FLEXO derived scripts may be :py:func:`check_clashes`, :py:func:`identify_dihedral`, :py:func:`calculate_rmsd`,
:py:func:`conformer_to_xyz` and :py:func:`conformer_to_mol2`.
"""
########################################################################
#                          MAIN FUNCTIONS                              #
########################################################################
import copy
import random
#For debug we use this seed:
#random.seed(1337)
#The MM energy of the best conformer of diphe.mol2 using the default configs.ini
#should be 83.6246 kcal/mol. And the SE energy of the best conformer of
#diphe_clashed.mol2 should be 42.9849 kcal/mol.

import numpy as np
try:
    from flexo.modules.cfunctions.cfunctions import check_clash_c
except ImportError:
    print("No pude cargar flexo_cfunctions en mainfunctions.py")
    from flexo.modules.cfunctions.cfunctions import check_clash_c
def calc_energy(conformerlst, molecule, calculator, **kwargs):
    """
    Run in parallel the energy calculations of a conformers' list.

    Parameters
    ----------
    conformerlst : list(MyConfomer)
        List of confomers to be optimized.
    molecule : MyMolecule
        Parental molecule.
    calculator : Calculator
        Calculator object to be used.
    pool : Pool
        Pool object to be used when running in parallel.

    Returns
    -------
    final_conformerlst : list(MyConformer)
        Conformers with energy calculated.
    """
    pool = kwargs["pool"]
    initial_conformerlst = conformerlst
    conformers_to_calculatelst = []
    not_to_calculate_confomerslst = []
    calculated_conformerlst = []
    symbol_list = molecule.atoms_properties["symbol"]
    atom_type_list = molecule.atoms_properties["atom_type"]
    atom_charge = molecule.atoms_properties["charge"]
    molecule_name = molecule.name
    original_bondlst = molecule.original_bondlst
    #Check if conformer has calculated energy or not.
    for conformer in initial_conformerlst:
        if conformer.energy is None:
            conformers_to_calculatelst.append(conformer)
        else:
            not_to_calculate_confomerslst.append(conformer)
    args_tuple = [(symbol_list,
                   conformer.atoms_coords,
                   index, atom_type_list,
                   atom_charge,
                   original_bondlst,
                   molecule_name) for index, conformer in enumerate(conformers_to_calculatelst)]
    energylst = pool.starmap(calculator.get_energy, args_tuple)

    for conformer, energy in zip(conformers_to_calculatelst, energylst):
        conformer.energy = energy
        calculated_conformerlst.append(conformer)
    final_conformerlst = not_to_calculate_confomerslst+calculated_conformerlst

    return final_conformerlst

def calculate_rmsd(conformer1_coords, conformer2_coords):
    """
    Return the all-atom root-mean-square deviation of atomic positions (RMSD) between two confomers.

    Paramteres
    ----------
    conformer1_coords : np.array
        Coordinates of the first confomer of the pair.
    conformer2 : np.array
        Coordinates of the second confomer of the pair.

    Returns
    -------
    kabsch_rmsd : float
        RMSD of two arrays of N x 3 after being rotated by the Kabsch algorithm.
    """
    P = conformer1_coords
    Q = conformer2_coords

    def kabsch(P, Q):
        """
        Return the rotation matrix according to the Kabsch algorithm.


        Parameters
        ----------
        P : np.array
            First array, to be rotated into Q
        Q : np.array
            Second array.

        Returns
        -------
        U : np.array
            Rotation matrix according of P into Q.
        """
        #We tried to use similar symbols to the ones found on
        #https://en.wikipedia.org/wiki/Kabsch_algorithm.
        A = (P.T).dot(Q)
        # Computation of the covariance matrix.
        try:
            V, S, W = np.linalg.svd(A)
        except np.linalg.LinAlgError as e:
            raise RuntimeError("SVD did not converge while calculating RMSD.")
        #Remember that det(A B) = det(A) det(B), and that det(A^T) = det(A)
        I = np.identity(3)
        I[2,2] = np.sign( np.linalg.det(V) * np.linalg.det(W) )
        U = ((W.T).dot(I)).dot(V.T)
        return(U)

    def rmsd(A, B):
        """
        Return the RMSD of two arrays of N x 3 (representing the x y z components of N atoms in each molecule)

        Parameters
        ----------
        A : np.array
            First array.
        B : np.array
            Second array.

        Returns
        -------
        rmsd : float
            RMSD of two arrays of N x 3
        """
        return(np.sqrt((3./A.size) * np.sum((A-B)**2)))

    def kabsch_rmsd(P, Q):
        """
        Calculate the RMSD after rotating the molecule by the Kabsch algorithm.

        Parameters
        ----------
        P : np.array
            First array.
        Q : np.array
            Second array.

        Returns
        -------
        kabsch_rmsd : float
            RMSD of two arrays of N x 3 after being rotated by the Kabsch algorithm.
        """
        #Find the matrix rotation for  P
        U = kabsch(P, Q)
        #Rotate P
        P = (U.dot(P.T)).T
        return rmsd(P, Q)

    return kabsch_rmsd(P, Q)


def energy_rmsd_filter(new_conformerlst, range_generate, max_final_conformers,
                    energy_threshold, rmsd_threshold, energy_limit):
    """
    Filter a list of generated conformers.

    Parameters
    ----------
    new_conformerlst : list
        List of new conformers.
    range_generate : bool
        True to generate a range (based on energy) of confomers or False to 
        generate a fixed final number of confomers.
    max_final_conformers : int
        Number of conformers to be generated.
    energy_threshold : float
        Energy threshold to consider two structures as different conformers.
    rmsd_threshold : float
        RMSD difference to consider two structures as different conformers.
    energy_limit : float
        Energy limit to set the range for range generation.


    Returns
    -------
    filtered_conformers : list
        List of selected conformers.
    """
    candidate_conformers = sorted(new_conformerlst, key=lambda x: x.energy)
    filtered_conformers = []
    filtered_conformers.append(candidate_conformers.pop(0))

    if range_generate is False:
        for candidate_conformer in candidate_conformers:
            selected_number = len(filtered_conformers)
            if selected_number < max_final_conformers:
                #If the energy difference between the last selected conformer
                #and the candidate conformer is above the energy threshold,
                #the conformers are different and the candidate is selected.
                energy_diff = (candidate_conformer.energy - filtered_conformers[-1].energy)
                if energy_diff > energy_threshold:
                    filtered_conformers.append(candidate_conformer)
                #If the energy difference is below the energy threshold,
                #the candidate could be geometrically identical with the
                #already selected conformers within the energy threshold
                #so it's necessary to check their RMSD.
                else:
                    acceptable = True
                    rmsd_list = []

                    for filtered_conformer in filtered_conformers[::-1]:
                        energy_diff = abs(candidate_conformer.energy - filtered_conformer.energy)
                        if energy_diff > energy_threshold:
                            break

                        rmsd = calculate_rmsd(filtered_conformer.atoms_coords,
                                              candidate_conformer.atoms_coords)
                        rmsd_list.append(rmsd)

                        if rmsd < rmsd_threshold:
                            acceptable = False
                            break

                    if acceptable is True:
                        filtered_conformers.append(candidate_conformer)
            else:
                break

    #If the range generator is on, take conformer in a range of energy
    #from lowest energy to lowest energy + energy_limit
    else:
        print(f"Using range generate.")
        candidate_number = len(candidate_conformers)
        selected_number = len(filtered_conformers)
        j = 1
        energy_diff_to_minima = abs(filtered_conformers[0].energy - candidate_conformers[j].energy)
        while energy_diff_to_minima < energy_limit:
            energy_diff = abs(filtered_conformers[-1].energy - candidate_conformers[j].energy)
            if energy_diff > energy_threshold:
                filtered_conformers.append(candidate_conformers[j])
            #If the energy difference is below the energy threshold,
            #the candidate could be geometrically identical with the
            #already selected conformers within the energy threshold
            #so it's necessary to check their RMSD.
            else:
                acceptable = True
                k = -1
                while energy_diff < energy_threshold:
                    rmsd = calculate_rmsd(filtered_conformers[k].atoms_coords,
                                                candidate_conformers[j].atoms_coords)
                    if rmsd < rmsd_threshold:
                        acceptable = False
                        break
                    k -= 1
                    #Check if the list index is valid.
                    if abs(k) > selected_number:
                        break
                    energy_diff = abs(filtered_conformers[k].energy - candidate_conformers[j].energy)
                if acceptable is True:
                    filtered_conformers.append(candidate_conformers[j])
            j += 1
            if j > candidate_number:
                break
            energy_diff_to_minima = abs(filtered_conformers[0].energy - candidate_conformers[j].energy)

    return filtered_conformers

def optimize_geom(conformerlst, molecule, calculator, **kwargs):
    """
    Run in parallel the optimization of geometry of a list of conformers.

    Parameters
    ----------
    conformerlst : list(MyConfomer)
        List of confomers to be optimized.
    molecule : MyMolecule
        Parental molecule.
    calculator : Calculator
        Calculator to be used.
    """
    pool = kwargs["pool"]
    symbol_list = molecule.atoms_properties["symbol"]
    out_format = kwargs["out_format"]
    range_generate = kwargs["range_generate"]
    max_final_conformers = kwargs["max_final_conformers"]
    energy_threshold = kwargs["energy_threshold"]
    rmsd_threshold = kwargs["rmsd_threshold"]
    energy_limit = kwargs["rmsd_threshold"]
    molecule_name = molecule.name
    conformerlst.sort(key=lambda x: x.energy)
    args_tuple = [(symbol_list,
                conformer.atoms_coords,
                index,
                molecule_name,
                out_format) for index, conformer in enumerate(conformerlst)]
    coords_energy = pool.starmap(calculator.get_opt, args_tuple)
    from modules.objects import MyConformer #TODO revisar este fideito.
    opt_conformers = [MyConformer(np.array(coords), energy=energy) for coords, energy in coords_energy]
    return energy_rmsd_filter(opt_conformers, range_generate, max_final_conformers,
                            energy_threshold, rmsd_threshold, energy_limit)


def generate_values(paramlst):
    """
    Generate new random values to be used in the construction of a Parameter object.

    Parameters
    ----------
    paramlst : list(Parameter)
        List of initial Parameter objects.

    Returns
    -------
    newparamlst : list(Parameter)
        New list of Parameter objects.
    """
    newparamlst = []
    for param in paramlst:
        newparam = param.newparam(random.uniform(param.min_value, param.max_value))
        newparamlst.append(newparam)

    return newparamlst


def identify_rotatable(molecule, mask):
    """
    Identify the rotatables bonds of a molecule.

    Parameters
    ----------
    molecule : MyMolecule
        MyMolecule object containing the molecule of interest.
    mask : str
        Index of two atoms which are going to be consider as the ONLY modifiable bond. For example, 2:4.

    Returns
    -------
    rotatablelst : list(int)
        List with atom indexes bonded by a rotatable bond.

    Raises
    ------
    OSError
        If there's something wrong with the .mol2 file.
    """
    rotatablelst = []

    connectionnumberlst = [0] * len(molecule.atoms_properties["symbol"])
    # find terminal atoms
    for atom1, atom2, bondorder in molecule.bondlst:
        connectionnumberlst[atom1-1] += 1
        connectionnumberlst[atom2-1] += 1

    for atom1, atom2, bondorder in molecule.bondlst:
        #atom1 = atom1-1
        #atom2 = atom2-1 #This -1 is here because a mol2 file starts from 1. SO UNCIVILIZED.
        if bondorder == 1 and (connectionnumberlst[atom1 - 1] > 1) and (connectionnumberlst[atom2 - 1] > 1):
            if (atom1, atom2) in molecule.bond_in_ringdic.keys():
                bond_in_ring = molecule.bond_in_ringdic[(atom1, atom2)]

            elif (atom2, atom1) in molecule.bond_in_ringdic.keys():
                bond_in_ring = molecule.bond_in_ringdic[(atom2, atom1)]
            else:
                raise OSError("Check the .mol2 file!")
            if bond_in_ring is False:
                if mask != "n:m":
                    for mask_i in mask.split(","):
                        minatom = int(mask_i.split(":")[0])
                        maxatom = int(mask_i.split(":")[1])
                        if (minatom <= atom1 <= maxatom) and (minatom <= atom2 <= maxatom):
                            rotatablelst.append([atom1, atom2])
                else:
                    rotatablelst.append([atom1, atom2])

    #Look for methyl groups and eliminate them.
    rotatablelst = [rotatable for rotatable in rotatablelst if (check_is_methyl(molecule, rotatable[0]) is False and check_is_methyl(molecule, rotatable[1]) is False)]
    return rotatablelst


def check_is_methyl(molecule, atomid):
    """
    Check if a carbon is part of a methyl group.

    Parameters
    ----------
    molecule : MyMolecule
        Molecule of interest.
    atomid : int
        Unique-id number of the atom.

    Return
    ------
    is_methyl : bool
        True if the carbon is part of a methyl, False if not.
    """
    sumCH = 0
    if molecule.atoms_properties["symbol"][atomid - 1] == "C":
        for i in range(0, len(molecule.connection_matrix[atomid - 1])):
            if molecule.connection_matrix[atomid - 1][i] == 1 and molecule.atoms_properties["symbol"][i] == "H":
                sumCH += 1
    if sumCH > 2:
        return True
    else:
        return False


def identify_dihedral(molecule, mask):
    """
    Identify the dihedral angles of a molecule.

    Parameters
    ----------
    molecule : MyMolecule
        Molecule of interest.
    mask : str
        Index of two atoms which are going to be consider as the ONLY modifiable bond. For example, 2:4.

    Returns
    -------
    dihedrallst  : list([int, int, int, int])
        List of lists containing the indexes of the dihedral angles.

    """
    weight_dict = {"H": 0, "O": 4, "N": 3, "C": 2}
    dihedrallst = []
    matrix = molecule.connection_matrix
    rotatablelst = identify_rotatable(molecule, mask)
    # bond between atom2 -- atom3
    for rotatable in rotatablelst:
        atom2 = rotatable[0]
        atom3 = rotatable[1]
        #Save atoms 1 and 4 in that order
        selectedatoms = []
        for thisatom in [atom2-1, atom3-1]:
            selectedatoms.append(None)
            #search other atoms connected to this
            otheratoms = np.nonzero(matrix[thisatom])[0]
            #Select atoms 1 and 4 with the priority: O>N>C>otros>H. A little arbitrary
            maxweight = -1
            for otheratom in otheratoms:
                otherweight = weight_dict.get(molecule.atoms_properties["symbol"][otheratom][0].upper(), 1)
                if otheratom + 1 not in [atom2, atom3]:
                    if otherweight > maxweight:
                            selectedatoms[-1] = otheratom
                            maxweight = otherweight
        atom1 = selectedatoms[0] + 1
        atom4 = selectedatoms[1] + 1
        #store dihedral
        dihedrallst.append([atom1, atom2, atom3, atom4])
    return dihedrallst

def check_clashes(conformer,molecule):
    """
    Check if the distance between two atoms is less that a threshold, if true a clash is detected.

    Parameters
    ----------
    conformer : Myconformer
        Conformer to be checked.

    Returns:
    is_clashed : bool
        True if a clash is detected, False if not.
    """

    xyz = conformer.atoms_coords
    xyz = xyz.ravel()
    df= molecule.distance_filter.ravel()
    return check_clash_c(xyz, df)


def conformer_to_xyz(conformer, molecule, conformer_file_path):
    """
    Convert a MyConfomer object to a .xyz file.

    Parameters
    ----------
    conformer : MyConformer
        Conformer to be converted to .xyz.
    molecule : MyMolecule
        Parental molecule.
    conformer_file_path : str
        Path where the conformer is going to be stored.
    """

    xyznamefile = conformer_file_path.split("/")[-1]

    with open(xyznamefile, "w") as xyzfile:
        if conformer.energy is not None:
            xyzstr = "{total_atoms}\nEnergy: {energy} kcal/mol\n".format(total_atoms=len(conformer.atoms_coords), energy=conformer.energy)
            for symbol, atom_coord in zip(molecule.atoms_properties["symbol"], conformer.atoms_coords):
                atoms_str = " ".join([str(line) for line in atom_coord])
                xyzstr += "{symbol} {coords}\n".format(symbol=symbol, coords=atoms_str)
            print(xyzstr, file=xyzfile)

def conformer_to_mol2(conformer, molecule, conformer_file_path=None):
    """
    Convert a MyConfomer object to a .mol2 file.

    Parameters
    ----------
    conformer : MyConformer
        Conformer to be converted to .mol2.
    molecule : MyMolecule
        Parental molecule.
    conformer_file_path : str
        Path where the conformer is going to be stored.
    """

    mol2_filename = conformer_file_path.split("/")[-1]
    conf_number = mol2_filename.split('_')[-1].split(".")[0]

    molecule_rti = molecule.molecule_rti.split('\n')

    if len(molecule_rti) > 4 and molecule_rti[4] == "":
        molecule_rti[4] = "****"
    molecule_internal_name = molecule_rti[0]
    molecule_rti[0] += "_"+conf_number
    molecule_rti.insert(5, f"Energy: {conformer.energy:.6f} kcal/mol")
    molecule_rti = '\n'.join(molecule_rti)

    mol2str = ["@<TRIPOS>MOLECULE\n",
            f"{molecule_rti}\n",
            "@<TRIPOS>ATOM\n"
            ]

    for unique_id, symbol, atom_coord, atom_type, charge in zip(molecule.atoms_properties["unique_id"],
                                                                molecule.atoms_properties["symbol"],
                                                                conformer.atoms_coords,
                                                                molecule.atoms_properties["atom_type"],
                                                                molecule.atoms_properties["charge"]
                                                                ):
        coords = [line for line in atom_coord]
        coords_str = [f"{coord: >10.4f}" for coord in coords]
        coords_str = "".join(coords_str)
        atom_line = f"{unique_id: >7} {symbol+str(unique_id): <8}{coords_str}"+\
                    f" {atom_type: <5}{1: >4}  {molecule_internal_name}_{conf_number}{charge: >10.4f}\n"
        mol2str.append(atom_line)
    mol2str.append("@<TRIPOS>BOND")
    bond_rti = [f"\n{str(index+1): >6}{bond[0]: >6}{bond[1]: >6}{bond[-1]: >5}"
        for index, bond in enumerate(molecule.original_bondlst)]
    mol2str.append("".join(bond_rti))
    mol2str = "".join(mol2str)
    with open(mol2_filename, "w") as mol2file:
        if conformer.energy is not None:
            print(mol2str, file=mol2file)

def get_dihedral(molecule, unique_id_list):
    """
    Return the dihedral angle between 4 atoms of a molecule.

    Parameters
    ----------
    molecule : MyMolecule
        Molecule of interest.
    unique_id_list : list, int
        unique_id of atom1, atom2, atom3 and atom4, which dihedral is going to be calculated.

    Returns
    -------
    dihedral_angle : float
        Dihedral angle between atom1, atom2, atom3 and atom4.
    """
    atom1 = unique_id_list[0]-1 #This -1 is here because a mol2 file starts from 1. SO UNCIVILIZED.
    atom2 = unique_id_list[1]-1
    atom3 = unique_id_list[2]-1
    atom4 = unique_id_list[3]-1

    v1 = molecule.initial_conformer.atoms_coords[atom2] - molecule.initial_conformer.atoms_coords[atom1]
    v2 = molecule.initial_conformer.atoms_coords[atom3] - molecule.initial_conformer.atoms_coords[atom2]
    v3 = molecule.initial_conformer.atoms_coords[atom4] - molecule.initial_conformer.atoms_coords[atom3]

    def mycross(x1,x2):
        """
        Return the cross product of two arrays.
        Faster than the numpy function for small arrays.
        """
        return np.array([x1[1]*x2[2]-x2[1]*x1[2],
                        -(x1[0]*x2[2]-x2[0]*x1[2]),
                        (x1[0]*x2[1]-x2[0]*x1[1])])

    vectorial_v1v2 = mycross(v1,v2)
    vectorial_v2v3 = mycross(v2,v3)
    unitary_v2 = v2/(np.linalg.norm(v2))

    a = np.dot(mycross(vectorial_v1v2, vectorial_v2v3), unitary_v2)
    b = np.dot(vectorial_v1v2,vectorial_v2v3)

    dihedral_angle = np.degrees(np.arctan2(a, b))
    return dihedral_angle


def find_connected(connection_matrix):
    """
    Use DFS algorithm to find all individuals fragments in a molecule.

    Parameters
    ---------
    connection_matrix : array_like
        Square matrix where a_ij is 0 if atoms i and j are not connected, or else the bond order.

    Returns
    -------
    n_fragmets: int
        Number of fragments found
    fragment_indexes: array_line
        Array where each index coresponds to an atom in the molecule, and indicates to which fragment it belongs.
    """
    total = list(range(len(connection_matrix)))
    fragments = []
    visited = []
    while total:
        to_visit = set((total[0],))
        fragments.append([])
        while to_visit:
            next_node = to_visit.pop()
            visited.append(next_node)
            fragments[-1].append(next_node)
            total.remove(next_node)
            for other, connection in enumerate(connection_matrix[next_node]):
                if connection != 0 and other not in visited:
                    to_visit.add(other)
    fragment_indexes = [0]*len(connection_matrix)
    for nfrag, frag in enumerate(fragments):
        for atom in frag:
            fragment_indexes[atom] = nfrag
    return len(fragments), np.array(fragment_indexes)
