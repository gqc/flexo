set(CMAKE_SHARED_LIBRARY_PREFIX "")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -fPIC")

#Rudimentary way to find Python3.8 modules. I'm pretty sure this doesn't
#work in Windows or iOS :S
set(NumPy_INCLUDE_DIRS ~/.local/lib/python3.8/site-packages/numpy/core/include)
if(NumPy_INCLUDE_DIRS)
    message(STATUS "Assuming Python modules to be in ~/.local/lib/python3.8/site-packages/")
    message(STATUS "NumPy found!")
    message(STATUS "Setting its include to ${NumPy_INCLUDE_DIRS}")
else()
    message(STATUS "Not a local user installation! Maybe modules are in /usr/local/lib/python3.8/site-packages/?")
    set(NumPy_INCLUDE_DIRS /usr/local/lib/python3.8/site-packages/numpy/core/include)
    if(NumPy_INCLUDE_DIRS)
            message(STATUS "NumPy found!")
            message(STATUS "Setting its include to ${NumPy_INCLUDE_DIRS}")
    else()
        message(FATAL_ERROR "NumPy not found! CMake will exit!")
    endif()
endif()

#Find Python. Could not find Numpy using that cmake module :(
find_package (Python3 3.8
              COMPONENTS Development REQUIRED)

#SWIG related stuff
find_package(SWIG REQUIRED)
include(${SWIG_USE_FILE})
#We need those headers
include_directories(${Python3_INCLUDE_DIRS}) #Python.h
include_directories(${CMAKE_CURRENT_SOURCE_DIR}) #cfunctions.h
include_directories(${NumPy_INCLUDE_DIRS}) #headers_varios_numpy.h

#We want a Python module from a C++ binary.
set_property(SOURCE cfunctions.i PROPERTY CPLUSPLUS ON)

set(CMAKE_SWIG_OUTDIR "${final_dir}")
swig_add_library(cfunctions TYPE SHARED LANGUAGE python SOURCES cfunctions.i cfunctions.cpp)

set_target_properties(cfunctions PROPERTIES LIBRARY_OUTPUT_DIRECTORY "${final_dir}")
include_directories(cfunctions PRIVATE ${Python3_INCLUDE_DIRS})
include_directories(cfunctions PRIVATE ${NumPy_INCLUDE_DIRS})
