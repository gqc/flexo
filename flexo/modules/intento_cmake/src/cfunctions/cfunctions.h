#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <stdlib.h>

void modify_dihedral(double *coords,
                     int size,
                     double *final_coords,
                     int a,
                     double *mask1,
                     int b,
                     double *mask2,
                     int c,
                     int id1,
                     int id2,
                     int id3,
                     int id4,
                     double theta);

bool check_clash_c(double *coord, 
                   int na, 
                   double *distance_filter, 
                   int nb);
