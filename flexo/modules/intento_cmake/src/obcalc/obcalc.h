#include <openbabel/obconversion.h>
#include <openbabel/mol.h>
#include <openbabel/forcefield.h>
#include <openbabel/math/vector3.h>

#include <iostream>

using namespace OpenBabel;


void optimize(double *coords, int size, double *final_coords, int final_size, long steps, char *molfile);
double teststruct(double *coords, int size, int a);
int setupCalc(char *molfile, char *forcefield);
double getEnergy(double *coords, int size);
