#include <openbabel/mol.h>
#include <openbabel/obconversion.h>
#include <openbabel/forcefield.h>
#include <openbabel/math/vector3.h>
#include <openbabel/builder.h>

#include <iostream>

using namespace OpenBabel;

OBMol *myMol;
OBForceField *myFF;
bool isCalcSetup;
char *mymolfile;

// Helper function to read molecule from file
OBMol *GetMol(char *molfile)
{

  // Create the OBMol object.
  OBMol *mol = new OBMol;

  // Create the OBConversion object.
  OBConversion conv;
  OBFormat *format = conv.FormatFromExt(molfile);
  if (!format || !conv.SetInFormat(format)) {
    std::cout << "Could not find input format for file " << molfile << std::endl;
    return mol;
  }

  // Open the file.
  std::ifstream ifs(molfile);
  if (!ifs) {
    std::cout << "Could not open " << molfile << " for reading." << std::endl;
    return mol;
  }
  // Read the molecule.
  if (!conv.Read(mol, &ifs)) {
    std::cout << "Could not read molecule from file " << molfile << std::endl;
    return mol;
  }

  return mol;
}

int setupCalc(char *molfile, char *forcefield)
{
  //~ printf("molfile: %s\n", molfile);
  //~ printf("ff: %s\n", forcefield);
  mymolfile = molfile;
  myMol = GetMol(molfile);

  myFF = OBForceField::FindForceField(forcefield);
  if (!myFF) {
    std::cout << "Could not find forcefield: " << forcefield << std::endl;
    return 0;
  }
  //~ // Setup the forcefield.
  if (!myFF->Setup(*myMol)) {
    std::cout << "Could not setup forcefield" << std::endl;
    return 0;
  }
  isCalcSetup=true;
  return 1;
}

double getEnergy(double *coords, int size){
    int atoms_number = size/3;
    // Check if calc has been setuped
    if (!isCalcSetup) {
        printf("Calculator not setted up, returning input coordinates!\n");
        return 0;
    }
    //~ std::cout << n << " " << myMol->NumAtoms() << std::endl;
    if ( atoms_number!= (myMol->NumAtoms())){
        std::cout << "Number of coordinates doesnt match number of atoms" << std::endl;
        std::cout << atoms_number  << " " << myMol->NumAtoms() << std::endl;
        return 0;
    }
    OBMol *tempMol = myMol;
    OBForceField *tempFF = myFF;
    
    tempMol->SetCoordinates(coords);
    tempFF->Setup(*tempMol);

    
    return tempFF->Energy();
}

void optimize(double *coords, int size, double *final_coords, int final_size, long steps, char *molfile){
    // Check if calc has been setuped
    if (!isCalcSetup) {
        printf("Calculator not setted up, returning input coordinates!\n");
    }
    // Initialize stuff
    OBMol *tempMol = myMol;
    OBForceField *tempFF = myFF->MakeNewInstance();
    tempFF->SetLogFile(&std::cout);
    tempFF->SetLogLevel(OBFF_LOGLVL_NONE);
    
    //Setup objects
    tempMol->SetCoordinates(coords);
    tempFF->Setup(*tempMol);
    //~ printf("MY COORD : ");
    //~ printf("%f\n", coords.coord[last]);
    //~ printf("COORD 0 0: ");
    //~ printf("%f\n", tempMol->GetCoordinates()[last]);
    
    if (tempMol->GetDimension() != 3) {
        
        OBBuilder _builder;
        _builder.Build(*tempMol);
        tempMol->AddHydrogens();
        
        tempMol->SetCoordinates(coords);
        tempFF->Setup(*tempMol);
    }
    
    //OPTIMIZE
    tempFF->SteepestDescent(steps);
    //~ tempFF->ConjugateGradients(steps);
    tempFF->GetCoordinates(*tempMol);
    
    double *newcoordinates;
    newcoordinates = tempMol->GetCoordinates();
    
    for (int i=0; i < size; i++) {
        final_coords[i] = newcoordinates[i];
    }
}

double teststruct(double *coords, int size, int a){
    if (a < size) {
        return coords[a];
    }
    return 0.;
}
