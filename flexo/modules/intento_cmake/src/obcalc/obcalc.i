%module obcalc
%{
#define SWIG_FILE_WITH_INIT
#include "obcalc.h"

%}
%begin %{
#define SWIG_PYTHON_CAST_MODE
%}

%include "numpy.i"
%init %{
import_array();
%}

%apply (double *INPLACE_ARRAY1, int DIM1) {(double *final_coords, int final_size)};
%apply (double *IN_ARRAY1, int DIM1) {(double *coords, int size)};

void optimize(double *coords, int size, double *final_coords, int final_size, long steps, char *molfile);
double teststruct(double *coords, int size, int a);
int setupCalc(char *molfile, char *forcefield);
double getEnergy(double *coords, int size);
