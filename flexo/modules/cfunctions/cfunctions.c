#include "stdio.h"
#include <stdbool.h>
#include <math.h>
#include <stdlib.h>

void dot(double *a,double *b,double *c,int n){
     for(int i=0;i<n;i++){
         for(int j=0;j<3;j++){
             
             
                c[3*i+j]=a[3*i+0]*b[3*0+j]+a[3*i+1]*b[3*1+j]+a[3*i+2]*b[3*2+j];
             
         }
     }
};

void cross(double *x1,double *x2,double *x3){
      
      x3[0]=(x1[1]*x2[2]-x2[1]*x1[2]);
      x3[1]=-(x1[0]*x2[2]-x2[0]*x1[2]);
      x3[2]=(x1[0]*x2[1]-x2[0]*x1[1]);
};

 double norm(double *v){
    return sqrt(v[0]*v[0]+v[1]*v[1]+v[2]*v[2]);

};

void modify_dihedral(double *coords,int size,double *final_coords,int a,double *mask1,int b,double *mask2,int c,int id1,int id2,int id3,int id4,double theta){
	int Natoms=size/3;
        double point[3]={0.,0.,0.};
        double normal[3]={0.,0.,0.};
        double v1[3]={0.,0.,0.};
        double v2[3]={0.,0.,0.};
	    double a23[3]={0.,0.,0.};
        //double a21[3]={0.,0.,0.};
        double *new_coords=malloc(3*Natoms*sizeof(double));
        //calculate new origin at at atom2
        
        for(int j=0;j<3;j++){
            point[j]=coords[3*id2+j];
            
        }
        //change coords to new origin
        for(int i=0;i<Natoms;i++){
           for(int j=0;j<3;j++){
               new_coords[3*i+j]=coords[3*i+j]-point[j];
           }
        }
        //calculate a23
        
        for(int i=0;i<3;i++){
            a23[i]=new_coords[3*id3+i]-new_coords[3*id1+i];   
        }
        
	    //calculate a21
        /*
        for(int i=0;i<3;i++){
            a21[i]=new_coords[3*id1+i]-new_coords[3*id2+i];   
        }
        */
        //calculate normal,cross product between a21 and a23
        
        for(int i=0;i<3;i++){
            v2[i]=new_coords[3*id3+i]-new_coords[3*id2+i];   
        }       
 
        //cross(a21,a23,normal);
        cross(v2,a23,normal); 
        //calculate normal v2
        /*
        for(int i=0;i<3;i++){
            v2[i]=new_coords[3*id3+i]-new_coords[3*id2+i];   
        }
        */
        cross(normal,v2,v1) ;
        
      

        
        //normalization of normal ,v1,v2
        double norm_normal=norm(normal);
        double norm_v1=norm(v1);
        double norm_v2=norm(v2);
   
        for (int i=0;i<3;i++){
            v1[i]=v1[i]/norm_v1; 
            v2[i]=v2[i]/norm_v2;
            normal[i]=normal[i]/norm_normal;
        }     
        //create A and A.T
        double A[9];
        double At[9];

        for (int i=0;i<3;i++){
            A[3*i+0]=normal[i];
            A[3*i+1]=v1[i];
            A[3*i+2]=v2[i];
        }

        
        for (int i=0;i<3;i++){
            At[3*0+i]=normal[i];
            At[3*1+i]=v1[i];               
            At[3*2+i]=v2[i];
            
        }

        



        //Transfor to the new coordinates
        
        dot(new_coords,A,final_coords,Natoms);

        double value=theta;
        if(mask1[3*id1+0]==0){
            value=-theta;

        }

        

        //calculate rotation matrix
        double coseno=cos(value);
        double seno=sin(value);
        
        double R[9]={coseno,seno,0.,-seno,coseno,0.,0.,0.,1.};

        
        //Apply masks to the fragments
        double *frag1=malloc(3*Natoms*sizeof(double));
        double *frag2=malloc(3*Natoms*sizeof(double));
            
        for(int i=0;i<Natoms*3;i++){
           frag1[i]=mask1[i]*final_coords[i];
           frag2[i]=mask2[i]*final_coords[i] ;

        }
        
        dot(frag1,R,new_coords,Natoms);
        
        for(int i=0;i<Natoms*3;i++){
            new_coords[i]=new_coords[i]+frag2[i];

        }

        dot(new_coords,At,final_coords,Natoms);
        
        for(int i=0;i<Natoms;i++)
           for(int j=0;j<3;j++){
               final_coords[3*i+j]=final_coords[3*i+j]+point[j];
        }
        free(frag1);
        free(frag2);
        free(new_coords);
        
        };

bool check_clash_c(double *coord,int na,double *distance_filter,int nb){
	int n=na/3;
        
	for (int i=0;i<n-1;i++){
		for (int j=i+1;j<n;j++){
		    float distance=sqrtf((coord[3*i+0]-coord[3*j+0])*(coord[3*i+0]-coord[3*j+0])+(coord[3*i+1]-coord[3*j+1])*(coord[3*i+1]-coord[3*j+1])+(coord[3*i+2]-coord[3*j+2])*(coord[3*i+2]-coord[3*j+2]));
	        //printf("%f\n",distance);
	            if (distance < distance_filter[n*i+j]) {
                            //printf("%d,%d\n",i,j);
			    return true;
			}		
	    }
	}
	return false;
	};





