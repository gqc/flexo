%module obcalc

%{
    struct coordinates {
        double* coord;
        int n;
    };
%}

struct coordinates {
    double* coord;
    int n;
};

%typemap(in) (double* coord, unsigned int) {
  int i;
  if (!PySequence_Check($input)) {
    PyErr_SetString(PyExc_ValueError,"Expected a sequence");
    return NULL;
  }
  int size = PySequence_Length($input);
  double *res;
  res = (double *) malloc(size*sizeof(double));
  for (i = 0; i < size; i++) {
    PyObject *o = PySequence_GetItem($input,i);
    if (PyNumber_Check(o)) {
      res[i] = PyFloat_AsDouble(o);
      /*printf("%f\n",res[i]);*/
    } else {
      PyErr_SetString(PyExc_ValueError,"Sequence elements must be numbers");      
      free(res);
      return NULL;
    }
  }
  $1 = res;
  $2 = size;
}
%typemap(freearg) double* {
   if ($1) free($1);
}

%typemap(in) struct coordinates (struct coordinates temp) {
  int i;
  if (!PySequence_Check($input)) {
    PyErr_SetString(PyExc_ValueError,"Expected a sequence");
    SWIG_fail;
  }
  int size = PySequence_Length($input);
  double *res;
  res = (double *) malloc(3*size*sizeof(double));
  for (i = 0; i < size; i++) {
    PyObject *atom = PySequence_GetItem($input,i);
    
    if (!PySequence_Check(atom)) {
      PyErr_SetString(PyExc_ValueError,"Expected a nx3 array");
      free(res);
      SWIG_fail;
    }
    int n;
    for (n=0; n<3; n++) {
      PyObject *o = PySequence_GetItem(atom, n);
      if (PyNumber_Check(o)) {
        res[i*3+n] = PyFloat_AsDouble(o);
      } else {
        PyErr_SetString(PyExc_ValueError,"Sequence elements must be numbers");      
        free(res);
        return NULL;
      }
    }
  }
  
  temp.coord = res;
  temp.n = size;
  $1 = temp;
}

%typemap(out) struct coordinates {
  int i;
  $result = PyList_New($1.n);                     
  for (i=0; i < $1.n; i++) {
    int n;
    PyObject *atomlist = PyList_New(3);
    for (n=0; n < 3; n++) {
      PyObject *o = PyFloat_FromDouble($1.coord[3*i+n]);
      PyList_SetItem(atomlist, n, o);             
    }
    PyList_SetItem($result, i, atomlist);             
  } 
}

%{
#include "obcalc.h"
%}

int setupCalc(char* molfile, char* forcefield);
double getEnergy(struct coordinates coord);
struct coordinates optimize(struct coordinates coord, long steps, char* molfile);
double teststruct(struct coordinates coords, int a);
