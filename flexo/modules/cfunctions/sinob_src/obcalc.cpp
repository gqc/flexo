#include <openbabel/mol.h>
#include <openbabel/obconversion.h>
#include <openbabel/forcefield.h>
#include <openbabel/math/vector3.h>
#include <openbabel/builder.h>

#include <iostream>

using namespace OpenBabel;

OBMol * myMol;
OBForceField *myFF;
bool isCalcSetup;
char* mymolfile;

//~ //Meme function to create CH4
//~ OBMol * molFromMymolecule()
//~ {
  //~ OBMol * mymol = new OBMol();
  //~ OBConversion conv;
  //~ std::ofstream ofs("mymetano.mol2");
  //~ conv.SetInAndOutFormats("MOL", "MOL2");
  
  //~ vector3 ccoord = vector3(-0.5746, -0.1252, -0.0000);
  //~ vector3 hcoord[4];
  
  //~ hcoord[0] = vector3( 0.5174, -0.1252,  0.0000);
  //~ hcoord[1] = vector3(-0.9386,  0.1732, -0.9854);
  //~ hcoord[2] = vector3(-0.9386,  0.5789,  0.7511);
  //~ hcoord[3] = vector3(-0.9386, -1.1278,  0.2343);
  
  //~ OBAtom * catom = mymol->NewAtom();
  //~ catom->SetAtomicNum(6);
  //~ catom->SetVector(ccoord);
  
  //~ OBAtom * hatom[4];
  //~ //OBBond * hbond[4];
  //~ for (int n=0; n<4; n++){
    //~ hatom[n] = mymol->NewAtom();
    //~ hatom[n]->SetAtomicNum(1);
    //~ hatom[n]->SetVector(hcoord[n]);
    //~ if (!mymol->AddBond(catom->GetIdx(), hatom[n]->GetIdx(), 1)){
      //~ std::cout << "Error" << std::endl;
    //~ }
    //~ //hbond[n] = mymol->NewBond();
    //~ //hbond[n]->Set(n, catom->GetId(), hatom[n]->GetId(), 1, 0);
  //~ }
  
  //~ conv.Write(mymol, &ofs);
  
  //~ std::cout << "Bonds: " << mymol->NumBonds() << std::endl;
  //~ return mymol;
//~ }



//////////////////////////////////////////////////////////////////////////////////
//// The real functions
//////////////////////////////////////////////////////////////////////////////////

struct coordinates {
    double* coord;
    unsigned int n;
};

// Helper function to read molecule from file
OBMol* GetMol(char* molfile)
{

  // Create the OBMol object.
  OBMol *mol = new OBMol;

  // Create the OBConversion object.
  OBConversion conv;
  OBFormat *format = conv.FormatFromExt(molfile);
  if (!format || !conv.SetInFormat(format)) {
    std::cout << "Could not find input format for file " << molfile << std::endl;
    return mol;
  }

  // Open the file.
  std::ifstream ifs(molfile);
  if (!ifs) {
    std::cout << "Could not open " << molfile << " for reading." << std::endl;
    return mol;
  }
  // Read the molecule.
  if (!conv.Read(mol, &ifs)) {
    std::cout << "Could not read molecule from file " << molfile << std::endl;
    return mol;
  }

  return mol;
}

int setupCalc(char* molfile, char* forcefield)
{
  //~ printf("molfile: %s\n", molfile);
  //~ printf("ff: %s\n", forcefield);
  mymolfile = molfile;
  myMol = GetMol(molfile);

  myFF = OBForceField::FindForceField(forcefield);
  if (!myFF) {
    std::cout << "Could not find forcefield: " << forcefield << std::endl;
    return 0;
  }
  //~ // Setup the forcefield.
  if (!myFF->Setup(*myMol)) {
    std::cout << "Could not setup forcefield" << std::endl;
    return 0;
  }
  isCalcSetup=true;
  return 1;
}

double getEnergy(struct coordinates coords){
    // Check if calc has been setuped
    if (!isCalcSetup) {
        printf("Calculator not setuped, returning input coordinates\n");
        return 0;
    }
    //~ std::cout << n << " " << myMol->NumAtoms() << std::endl;
    if ( coords.n != (myMol->NumAtoms())){
        std::cout << "Number of coordinates doesnt match number of atoms" << std::endl;
        std::cout << coords.n << " " << myMol->NumAtoms() << std::endl;
        return 0;
    }
    OBMol* tempMol = myMol;
    OBForceField* tempFF = myFF;
    
    tempMol->SetCoordinates(coords.coord);
    tempFF->Setup(*tempMol);

    
    return tempFF->Energy();
}

struct coordinates optimize(struct coordinates coords, long steps, char * molfile){
    // Check if calc has been setuped
    if (!isCalcSetup) {
        printf("Calculator not setuped, returning input coordinates\n");
        return coords;
    }
    // Initialize stuff
    OBMol *tempMol = myMol;
    
    OBForceField* tempFF = myFF->MakeNewInstance();
    
    // Coments for debugging (?
    //~ const char* f = "GAFF";
    //~ const char* m = "/home/lean/Proyectos/flexo/testeo/ccalctest/ccalc/187244315_cmm_conformers/187244315_conformer0.xyz";
    //~ OBMol* tempMol = GetMol((char*)m);
    //~ OBMol* tempMol = GetMol(molfile);
    
    //int last = 3 * coords.n - 1;
    //~ OBForceField* tempFF = OBForceField::FindForceField(f);
    //~ printf("COORD 0 0 PRE: ");
    //~ printf("%f\n", tempMol->GetCoordinates()[last]);
    //~ printf("NCONFS: %i\n", tempMol->NumConformers());
    tempFF->SetLogFile(&std::cout);
    //~ tempFF->SetLogLevel(OBFF_LOGLVL_LOW);
    tempFF->SetLogLevel(OBFF_LOGLVL_NONE);
    //~ tempFF->SetUpdateFrequency(100);
    
    //Setup objects
    tempMol->SetCoordinates(coords.coord);
    tempFF->Setup(*tempMol);
    //~ printf("MY COORD : ");
    //~ printf("%f\n", coords.coord[last]);
    //~ printf("COORD 0 0: ");
    //~ printf("%f\n", tempMol->GetCoordinates()[last]);
    
    if (tempMol->GetDimension() != 3) {
        
        OBBuilder _builder;
        _builder.Build(*tempMol);
        tempMol->AddHydrogens();
        
        tempMol->SetCoordinates(coords.coord);
        tempFF->Setup(*tempMol);
    }
    
    //OPTIMIZE
    tempFF->SteepestDescent(steps);
    //~ tempFF->ConjugateGradients(steps);
    tempFF->GetCoordinates(*tempMol);
    
    coords.coord = tempMol->GetCoordinates();
    return coords;
}

double teststruct(struct coordinates coords, int a){
    if (a < coords.n * 3) {
        return coords.coord[a];
    }
    return 0.;
}
