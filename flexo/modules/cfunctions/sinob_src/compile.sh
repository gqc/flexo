mkdir build || exit 0
cd build || exit 0

cp ../obcalc.* ./
swig -python obcalc.i
g++ -fPIC -Wall -std=c++11 -c obcalc.cpp obcalc_wrap.c -I /usr/local/include/openbabel3/ -I /usr/include/python3.8
g++ -L/usr/local/lib/ -Wall -shared -o _obcalc.so obcalc.o obcalc_wrap.o -lopenbabel
#g++ -fPIC -Wall -std=c++11 -c obcalc.cpp obcalc_wrap.c -I /home/lean/sources/openbabel-openbabel-3-1-1/include/ -I /home/lean/sources/openbabel-openbabel-3-1-1/build/include/  -I /usr/include/python3.8
#g++ -L/home/lean/sources/openbabel-openbabel-3-1-1/build/lib/ -Wall -shared -o _obcalc.so obcalc.o obcalc_wrap.o -lopenbabel

python3 ../test.py

echo "finished"
