#include <openbabel/obconversion.h>
#include <openbabel/mol.h>
#include <openbabel/forcefield.h>
#include <openbabel/math/vector3.h>

#include <iostream>

using namespace OpenBabel;

//struct coordinates {
    //double* coord;
    //int n;
//};

//OBMol* GetMol(const std::string &filename);
//double openAndGetE(char* filename);
//double getEnergy(OBMol* mol, char* forcefield);
//OBMol * molFromMymolecule();
//int main(int argc, char **argv);

//double getMyEnergy(const char* molfile, char* forcefield);
//double getEnergy();
int setupCalc(char* molfile, char* forcefield);
double getEnergy(struct coordinates coord);
struct coordinates optimize(struct coordinates coord, long steps, char* molfile);
//int move_coords(double *coord, unsigned int n);
double teststruct(struct coordinates coords, int a);
