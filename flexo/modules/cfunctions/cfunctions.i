%{
#define SWIG_FILE_WITH_INIT
#include "cfunctions.h"
%}
%begin %{
#define SWIG_PYTHON_CAST_MODE
%}
%module cfunctions
 %{
%}

%include "numpy.i"
%init %{
import_array();
%}
%apply (double* IN_ARRAY1,int DIM1) {(double *coords,int size),(double *mask1,int b),(double *mask2,int c),(double *coord,int na)};
%apply (double* INPLACE_ARRAY1,int DIM1){(double *final_coords,int a)} 
%apply (double* IN_ARRAY1,int DIM1){(double *coord,int na),(double *distance_filter,int nb)}
%include "cfunctions.h"

