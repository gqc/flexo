import random
import sys
import time
from math import inf, sqrt
from copy import deepcopy
#For debug we use this seed:
#random.seed(1337)
#The MM energy of the best conformer of diphe.mol2 using the default configs.ini
#should be 83.6246 kcal/mol. And the SE (MOPAC2016) energy of diphe_clashed.mol2 should be
#42.9654 kcal/mol.
from flexo.modules import mainfunctions
from flexo.modules import objects

from math import factorial
import copy
from itertools import product
###############################
# GENETIC ALGORITHM INTERFACE #
###############################

class SetsGeneration:
    """
    This class is used to connect with the genetic library.

    Attributes:
        memberlst (list, Member): List of member objects.
        minlst (list, float): List with minimum values with same type of Parameter chromosome.
        maxlst (list, float): List with maximum values with same type of Parameter chromosome.
        chromosomelst (list, Parameter): List containing Parameter chromosomes.
        fitnesslst (list, float): List List containing fitness values.

    Special methods:
        __next__
    """

    def __init__(self, conformerlst, molecule):
        """
        Constructor for SetsGeneration class.

        Parameters:
            conformerlst (list, MyConformer): List of MyConfomers.
            molecule (MyMolecule): Parental molecule.
        """
        self.conformerlst = conformerlst
        self.molecule = molecule
        minlst = []
        maxlst = []
        #Take min and max values of the first member
        for param in conformerlst[0].paramlst:
            minlst.append(param.min_value)
            maxlst.append(param.max_value)
        chromosomelst = []
        fitnesslst = []
        for conformer in self.conformerlst:
            fitnesslst.append(conformer.fitness)
            chromosome = []
            for param in conformer.paramlst:
                chromosome.append(param)
            chromosomelst.append(chromosome)
        self.minlst = minlst
        self.maxlst = maxlst
        self.chromosomelst = chromosomelst
        self.fitnesslst = calculate_fitness(conformerlst)

    def __next__(self):
        """
        Return a list of dictionaries with new parameter values.
        """
        ga = Generation(self.chromosomelst, self.fitnesslst,
                        self.minlst, self.maxlst)
        newgeneration = next(ga)
        newmemberlst = []
        newparamlst = []

        param=self.conformerlst[0].paramlst

        for index in range(len(param)):
            paramtype = param[index].paramtype
            atoms = param[index].atoms
            min_value = param[index].min_value
            max_value = param[index].max_value
            mask_frag = param[index].masks
            newparamlst.append(objects.Parameter(paramtype, atoms, 0., min_value,
                                                     max_value, mask_frag))


        for newmember in newgeneration:
            
            newvalueslst = newmember.get_chrom()
            for index in range(len(param)):
                newparamlst[index].value=newvalueslst[index]               


            newconformer = self.molecule.create_new_conformer(deepcopy(newparamlst))
            if mainfunctions.check_clashes(newconformer,self.molecule) is False:
                newmemberlst.append(newconformer)
        #Compare input list of members (memberlst) with the new list of members (newmemberlst), to avoid calculating again the energy of old members.
        for member2 in newmemberlst:
            for member1 in self.conformerlst:
                check = 0
                for param1, param2 in zip(member1.paramlst, member2.paramlst):
                    if param2.value == param1.value:
                        check += 1
                if check == len(member2.paramlst):
                    member2.energy = member1.energy
                    member2.atoms_coords = member1.atoms_coords
        return newmemberlst


def calculate_fitness(conformers):
    """
    Calculate fitness for a list of Member.
    Fitness formula based on J. A. Niesse and Howard R. Mayne, The Journal of Chemical Physics 105:11, 4700-4706 (https://doi.org/10.1063/1.472311 ) with a modification made by us.


    Parameters:
        memberlst (list, MyConformer): List of MyConformers which fitness are desired.

    Returns:
        fi_list(list, float): Fitness of the different MyConformers.
    """
    fi_list = []
    energy_list = []
    Fi_list = []

    conformers_with_energy = [conf for conf in conformers if conf.energy is not None]
    for conformer in conformers_with_energy:
        energy_list.append(conformer.energy)
    min_energy = min(energy_list)
    max_energy = max(energy_list)
    deltaE = (max_energy - min_energy)
    for energy in energy_list:
        try:
            Fi = (max_energy - energy) / deltaE
            Fi_list.append(Fi)
        except ZeroDivisionError:
            print("ERROR in calculate_fitness()! deltaE = 0")
            sys.exit()
    Ftot = sum(Fi_list)
    for Fi in Fi_list:
        fi_list.append(Fi / Ftot)
    for conformer, fitness in zip(conformers_with_energy, fi_list):
        conformer.fitness = fitness
    return fi_list


###################
# GENETIC LIBRARY #
###################

class Generation:
    """
    The Generation object is used as an easy interface to interact with the library.

    Attributes:
        memberlst (list, Member): List of members in the generation.
        vallstlst (list, list): List of list with values of chromosomes.
        fitnesslst (list, float): List of fitness linked to vallstlst.
        minlst (list, float): List with minimum values with same form of chromosome.
        maxlst (list, float): List with maximum values with same form of chromosome.

    Methods:
        build_member

    Special methods:
        __next__
    """

    def __init__(self, vallstlst=None, fitnesslst=None, minlst=None, maxlst=None):
        """
        Constructor for Generation class.

        Parameters:
            vallstlst (list, list): List of list with values of chromosomes.
            fitnesslst (list, float): List of fitness linked to vallstlst.
            minlst (list, float): List with minimum values with same form of chromosome.
            maxlst (list, float): List with maximum values with same form of chromosome.
        """
        if vallstlst is None:
            self.vallstlst = []
        if fitnesslst is None:
            self.fitnesslst = []
        self.memberlst = []
        self.fitnesslst = fitnesslst
        self.minlst = minlst
        self.maxlst = maxlst

        for i in range(0, len(vallstlst)):
            self.build_member(vallstlst[i], fitnesslst[i], minlst, maxlst)

    def build_member(self, vallst, fitness, minlst=None, maxlst=None):
        """
        Build a new Member for the Generation.

        Parameters:
            vallst (list): List of values of chromosomes.
            fitness (float): Fitness linked to vallst.
            minlst (list, float): List with minimum values with same form of chromosome.
            maxlst (list, float): List with maximum values with same form of chromosome.
        """
        chromosome = []
        if minlst is None:
            minlst = self.minlst
        if maxlst is None:
            maxlst = self.maxlst
        for i in range(len(vallst)):
            val = vallst[i]
            try:
                minval = minlst[i]
            except IndexError:
                minval = None
            try:
                maxval = maxlst[i]
            except IndexError:
                maxval = None
            gene = Gene(val, minval, maxval)
            chromosome.append(gene)
        member = Member(chromosome,fitness)
        self.memberlst.append(member)

    def __next__(self):
        """
        Set the GeneticAlgorithm and run it.

        Returns:
           ga.run_ga(): Members parents + Members children.
        """
        ga = GeneticAlgorithm(self.memberlst)
        return ga.run_ga()


class GeneticAlgorithm:
    """
    This class contains the main functions of the genetic library.

    Attributes:
        ALTERCONST
        memberlst
        MUTPERCENT
        NEIGHBORPERCENT
        TOURNAMENTWEAKWINPROB
        ELITISMPERCENT

    Methods:
        check_limits
        run_ga
        selection_parent
        weighted_selection
        build_randomchildren
        crossover_chromosome
        crossover_gene
        crossover_lstgene
        crossover_floatgene
        mutation_gene
        mutation_lstgene
        mutation_floatgene
        neighbor_chromosome
        neighbor_gene
        neighbor_floatgene
        neighbor_lstgene
    """

    def __init__(self, memberlst):
        """
        Constructor for the GeneticAlgorithm class.

        Parameters:
            memberlst (list, Member): List of members to start the genetic algorithm.
        """
        self.ALTERCONST = 0.2 #MAGIC NUMBER!
        self.memberlst = memberlst
        self.MUTPERCENT = 0.2  #MAGIC NUMBER!
        self.NEIGHBORPERCENT = 0.1 #MAGIC NUMBER!
        self.TOURNAMENTWEAKWINPROB = 0.05 #MAGIC NUMBER!
        self.ELITISMPERCENT = 0.2 #MAGIC NUMBER!


    def check_limits(self, val, minval, maxval):
        """
        Check if a value is between two given limits.

        Parameters:
            val (float): Number to be checked.
            minval (float): Lower limit.
            maxval (float): Upper limit.

        Returns:
            val (float): Value that passed the check.
        """
        if val < minval:
            return minval
        elif val > maxval:
            return  maxval
        else:
            return val


    def run_ga(self):
        """
        Run the genetic algorithm.

        Returns:
            (list, Member)
        """
        #Get half the number of members in the generation as parents for the next generation...
        parents = self.selection_parent()
        #...and complete with children.
        children = self.build_randomchildren(parents)
        return parents + children


    #######################
    # SELECTION FUNCTIONS #
    #######################

    def selection_parent(self):
        return self.weighted_selection()

    def weighted_selection(self):
        """
        Pick Members to be parents using a weighted (by Member fitness) random
        selection.

        Returns:
            parents (list, Member): Members selected to be parents.
        """
        members = sorted(self.memberlst, key=lambda x: x.fitness, reverse=True)
        num_elite = int(len(self.memberlst)*self.ELITISMPERCENT)
        num_non_elite = len(self.memberlst)//2-num_elite
        elite_members = members[:num_elite]
        del members[:num_elite]
        fitnesses = [member.fitness for member in members]
        non_elite_members = random.choices(members, weights=fitnesses, k=num_non_elite)
        parents = elite_members + non_elite_members
        return parents

    def build_randomchildren(self, parentlst):
        """
        Return half of the number of parents as new children Member objects.

        Parameters:
        parentlst (list, Member): List of selected parents.

        Returns:
            childrenlst (list, Member): List of new Member objects.
        """
        childrenlst=[]
        #copy chromosome list
        while len(childrenlst) != len(parentlst):
            #build a child near a parent (neighbor child)
            if random.random() < self.NEIGHBORPERCENT: #esto no se como funciona.
                #select the parent
                parent = random.choice(parentlst)
                childrenlst.append(self.neighbor_chromosome(parent.chromosome))
            #build a child from crossingover its parents chromosomes.
            else:
                #select the parent 1
                parent1 = random.choice(parentlst)
                #select the parent 2
                parent2 = random.choice(parentlst)
                #make a new child
                child = self.crossover_chromosome(parent1.chromosome,
                    parent2.chromosome)
                childrenlst.append(child)
        return childrenlst


    #######################
    # CROSSOVER FUNCTIONS #
    #######################

    def crossover_chromosome(self, chromosome1, chromosome2):
        """
        Crossover two chromosomes.

        Parameters:
            chromosome1 (list, Gene): List of Gene objects of the first chromosome.
            chromosome2 (list, Gene): List of Gene objects of the second chromosome.

        Returns:
            Member(childgenlst)
        """
        childgenlst = []
        for igen in range(0,len(chromosome1)):
            crossgen = self.crossover_gene(chromosome1[igen],chromosome2[igen])
            # mutation of gen
            crossgen = self.mutation_gene(crossgen)
            childgenlst.append(crossgen)
        return Member(childgenlst)


    def crossover_gene(self, gene1, gene2):
        """
        Interchange genes, redirection depends of the type of the gene.

        Parameters:
            gene1 (Gene): First gene to be interchanged.
            gene2 (Gene): Second gene to be interchanged.

        Returns:
            New gene object.

        Raises:
            TypeError: If the genes have different type.
            TypeError: If the type of the gene is not defined.
        """
        if type(gene1.val)!=type(gene2.val):
            if "float" in (str(type(gene1.val))) and ("float" in str(type(gene2.val))):
                pass
            else:
                raise TypeError("ERROR: Genes with different types!")
        if isinstance(gene1.val,float):
            return self.crossover_floatgene(gene1, gene2)
        elif isinstance(gene1.val, list):
            return self.crossover_lstgene(gene1, gene2)
        else:
            raise TypeError("ERROR! Type of gene is not defined!")


    def crossover_lstgene(self, lstgene1, lstgene2):
        """
        Crossover two list type genes.

        Parameters:
            lstgene1 (Gene): First Gene object with list as value.
            lstgene2 (Gene): Second Gene object with list as value.

        Returns:
            lstnewgene (Gene): A gene object with list as value.
        """
        rndselector = random.random()
        # gaussian center in 0 x ALTERCONST
        rndalter = random.gauss(0,0.3) * self.ALTERCONST
        newgenelst = []
        # random number select one of the two input list of genes
        if rndselector < 0.5:
            lstgeneselect = lstgene1
        else:
            lstgeneselect = lstgene2
        # iterate over all genes in the selected list of genes
        valuelst = []
        for i in range(len(lstgeneselect.val)):
            minval = lstgeneselect.minval[i]
            maxval = lstgeneselect.maxval[i]
            value = lstgeneselect.val[i]
            newval = self.check_limits(value + value * rndalter, minval, maxval)
            valuelst.append(newval)
        lstnewgene = Gene(valuelst, lstgeneselect.minval, lstgeneselect.maxval)
        return lstnewgene


    def crossover_floatgene(self, floatgene1, floatgene2):
        """
        Crossover genes with float values.

        Parameters:
            floatgene1 (Gene): First Gene object with float value.
            floatgene2 (Gene): Second Gene object with float value

        Returns:
            (Gene): New Gene object.
        """
        rndselector = random.random()
        # gaussian center in 0 x ALTERCONST
        rndalter = random.gauss(0,0.3) * self.ALTERCONST

        if rndselector < 0.5:
            newval = self.check_limits(floatgene1.val + floatgene1.val * rndalter,floatgene1.minval,floatgene1.maxval)
            return Gene(newval,floatgene1.minval,floatgene1.maxval)
        else:
            val = floatgene2.val + floatgene2.val * rndalter
            minval = floatgene2.minval
            maxval = floatgene2.maxval
            # check if value is between limits
            newval = self.check_limits(val,minval,maxval)
            return Gene(newval,floatgene2.minval,floatgene2.maxval)


    ######################
    # MUTATION FUNCTIONS #
    ######################

    def mutation_gene(self, gene):
        """
        Mutate a gene depending on the type of its value.

        Parameters:
            gene (Gene): Gene object to be mutated.

        Returns:
            gene (Gene): Mutated Gene object.
        """
        # do mutation on val
        rnd = random.random()
        if rnd > self.MUTPERCENT:
            return gene
        else:
            if isinstance(gene.val,float):
                gene.val = (self.mutation_floatgene(gene))
                return gene
            elif isinstance(gene.val,list):
                gene.val = (self.mutation_lstgene(gene))
                return gene


    def mutation_lstgene(self, lstgene):
        """
        Mutate a gene with list value.

        Parameters:
            lstgene (Gene): Gene object with list as value.

        Returns:
            mutvallst (list): List of new values restricted by minval a maxval.
        """
        mutvallst = []
        for i in range(len(lstgene.val)):
            newval = random.uniform(lstgene.minval[i], lstgene.maxval[i])
            mutvallst.append(newval)
        return mutvallst


    def mutation_floatgene(self, floatgene):
        """
        Mutate a gene with float value.

        Parameters:
            floatgene (Gene): Gene object with float as value.

        Returns:
            mutval (float): New value restricted by minval a maxval.
        """
        minval = floatgene.minval
        maxval = floatgene.maxval
        mutval = random.uniform(minval,maxval)
        return mutval

    ######################
    # NEIGHBOR FUNCTIONS #
    ######################

    def neighbor_chromosome(self, chromosome):
        """
        Build a neighbor chromosome from a parent chromosome modifying a little the input chromosome.

        Parameters:
            chromosome (list, Gene): List of Gene objects.

        Returns:
            (list, Gene): List of new gene objects
        """
        childgenlst = []
        for igen in range(0,len(chromosome)):
            neighborgen = self.neighbor_gene(chromosome[igen])
            childgenlst.append(neighborgen)
        return Member(childgenlst)


    def neighbor_gene(self, gene):
        """
        Build a neighbor gene, redirection depends of the type of gene.

        Parameters:
            gene (Gene): Gene object.

        Returns:
            (Gene): New gene object.

        Raises:
            TypeError: If the type of the gene is not defined.
        """
        if isinstance(gene.val,float):
            return self.neighbor_floatgene(gene)
        elif isinstance(gene.val,list):
            return self.crossover_lstgene(gene)
        else:
            # print gene.val
            raise TypeError("ERROR: Type of gene is not defined")


    def neighbor_floatgene(self, floatgene):
        """
        Return a slightly modified float gene.

        Parameters:
            floatgene (Gene): Gene object with float as value.

        Returns:
            (Gene): New gene.
        """
        rndselector = random.random()
        # gaussian center in 0 x ALTERCONST
        rndalter = random.gauss(0,0.3) * self.ALTERCONST

        newval = self.check_limits(floatgene.val + floatgene.val * rndalter,floatgene.minval,floatgene.maxval)
        return Gene(newval,floatgene.minval,floatgene.maxval)


class Member:
    """
    The Member class is an individue in the genetic algorithm.

    Parameters:
        chromosome (list, Gene): List of genes of the individue.
        fitness (float): Fitness of the individue.

    Methods:
        get_chrom
    """

    def __init__(self, genelst, fitness=None):
        """
        Constructor for the Member class.

        Parameters:
             genelst (list, Gene): List of genes of the individue.
             fitness (float): Fitness of the individue.
        """
        self.chromosome = genelst
        self.fitness = fitness

    def get_chrom(self):
        """
        Return the value of the genes in the chromosome of the individue.

        Returns:
            genevallst(list): List containing the values of the genes of the individue.
        """

        genevallst = []
        for gene in self.chromosome:
            genevallst.append(gene.val)
        return genevallst

class Gene:
    """
    The smallest object of the genetic algorithm. Used to store the parameters to be modified.

    Attributes:
        val (float or list of float): Value of the gene.
        minval (float or list of float): Minimum value of the gene.
        maxval (float or list of float): Maximum value of the gene.
    """

    def __init__(self, val, minval=None, maxval=None):
        """
        Parameters:
            val (float or list of float): Value of the gene.
            minval (float or list of float): Minimum value of the gene.
            maxval (float or list of float): Maximum value of the gene.
        """
        self.DEFLIMIT = 180.0 #MAGIC NUMBER!
        self.val = val

        if minval is None:
            if isinstance(val, float):
                self.minval = (val - self.DEFLIMIT)
            elif isinstance(val, list):
                #build a list of dimension of gene
                self.minval = ([val - self.DEFLIMIT]*len(val))
        else:
            self.minval = minval
        if maxval is None:
            if isinstance(val, float):
                self.maxval = (val + self.DEFLIMIT)
            elif isinstance(val, list):
                #build a lst of dimension of gene
                self.maxval = ([val + self.DEFLIMIT]*len(val))
        else:
            self.maxval = maxval


################################
# GENETIC ALGORITHM EXPLORATOR #
################################

class GAExplorator:
    """
    This class is used to run the genetic algorithm optimizator.

    Attributes:
        molecule (MyMolecule): Molecule which confomers are of interest.
        calculator (Calculator): Calculator used to evaluate the energy of the confomers.
        quiet (bool): True to print to console, False to run quietly.
        max_gen (int): Number of generations to run the genetic algorithm.
        max_final_conformers (int): Number of conformers to be generated.
        members_number (int): Number of members to run the genetic algorithm.
        pool (Pool): Pool object to be used when running in parallel.
        energy_threshold (float): Energy threshold to consider two structures as different conformers.
        rmsd_threshold (float): RMSD difference to consider two structures as different conformers.
        range_generate (bool): True to generate a range (based on energy) of
                               confomers or False to generate a fixed final number of confomers.
        energy_limit (float): Energy limit to set the range for range generation.

    Methods:
        get_conformers
    """

    def __init__(self, molecule, calculator, **kwargs):
        """
        Constructor for the GAExplorator class.

        Parameters:
            molecule (MyMolecule): Molecule which confomers are of interest.
            calculator (Calculator): Calculator used to evaluate the energy of the confomers.

        Arbitrary Keyword Args:
            quiet (bool): True to print to console, False to run quietly.
            max_gen (int): Number of generations to run the genetic algorithm.
            max_final_conformers (int): Number of conformers to be generated.
            members_number (int): Number of members to run the genetic algorithm.
            pool (Pool): Pool object to be used when running in parallel.
            energy_threshold (float): Energy threshold to consider two structures as different conformers.
            rmsd_threshold (float): RMSD difference to consider two structures as different conformers.
            range_generate (bool): True to generate a range (based on energy) of
                                   confomers or False to generate a fixed final
                                   number of confomers.
            energy_limit (float): Energy limit to set the range for range generation.
        """
        self.molecule = molecule
        self.calculator = calculator
        self.quiet = kwargs["quiet"]
        self.max_gen = kwargs["max_gen"]
        self.max_final_conformers = kwargs["max_final_conformers"]
        self.members_number = kwargs["members_number"]
        self.pool = kwargs["pool"]
        self.energy_threshold = kwargs["energy_threshold"]
        self.rmsd_threshold = kwargs["rmsd_threshold"]
        self.range_generate = kwargs["range_generate"]
        self.energy_limit = kwargs["energy_limit"]

        self.save_all = kwargs.get("save_all", False)

        self.convergence_counter_limit = 3

    def get_conformers(self):
        """
        Return a list of the best generated conformers.

        Returns:
            bestconfomers_data (tuple: list, float, float):  Tuple with the best conformers'
                                                             data obtained by the genetic algorithm.
                                                             Found MyConformers, energy of the best
                                                             and average energy of the final assembly.
        """
        def _meteor_strike(conformers):
            """
            Kill 2/3 of the conformer population, trying to avoid or to
            break out of a premature convergence.
            Just the best conformer of the generation is guaranteed to survive.

            Parameters:
                conformers (list: MyConformer): Conformer population to be quelled.

            Returns:
                survivors (list: MyConformer): Surviving conformers.
            """

            conformers.sort(key=lambda x: x.energy)
            survivors_number = len(conformers)//3-1
            survivors = conformers[:1] + random.sample(conformers[1:], k=survivors_number)
            return survivors

        ##Cycle initialization
        bestconformers = []
        conformerlst = []
        generation_convergence_counter, current_average, scorched_earth = 0, None, False

        if self.save_all:
            clashed_conformerlst = []
            outconformerfilename = f"./outgafile_{self.molecule.name}.log"
            outconformerfile = open(outconformerfilename, "w")
            outbestconformerfilename = f"./outbestgafile_{self.molecule.name}.log"
            outbestconformerfile = open(outbestconformerfilename, "w")

        #Cycle start
        for cycle_number in range(1, self.max_gen+1):
            startcycle_time = time.time()
            #Generate a new conformer if one had clashes and was eliminated
            clash_counter = 0
            while len(conformerlst) < self.members_number:
                #Use the initial conformer to generate a new member
                new_paramlst = mainfunctions.generate_values(self.molecule.initial_paramlst)
                new_conformer = self.molecule.create_new_conformer(new_paramlst)
                if clash_counter == 1000000:
                    print("Check initial mol2!")
                    print(f"Tried to generate {clash_counter} random conformers and failed!")
                    sys.exit(1)
                if mainfunctions.check_clashes(new_conformer,self.molecule) is False:
                    conformerlst.append(new_conformer)
                    clash_counter = 0
                else:
                    if self.save_all:
                        new_conformer.energy = inf
                        clashed_conformerlst.append(new_conformer)
                    del new_paramlst
                    del new_conformer
                    clash_counter += 1
            #Update energy
            if self.calculator.__class__.__name__ == "MopacCalculator":
                if not self.quiet:
                    print("\nMOPAC is running...")
            conformerlst = mainfunctions.calc_energy(conformerlst, self.molecule,
                                                    self.calculator, pool=self.pool)

            bestconformerlst = mainfunctions.energy_rmsd_filter(conformerlst+bestconformers,
                                            self.range_generate, self.max_final_conformers,
                                            self.energy_threshold, self.rmsd_threshold, self.energy_limit)
            bestconformers = bestconformerlst
            bestconf_energy = sorted([bestconf.energy for bestconf in bestconformerlst])[0]
            average_energy_bestconf = sum([bestconf.energy for bestconf in bestconformerlst])/len(bestconformerlst)

            # Save conformers before converge assesement.
            if self.save_all:
                for conformer in conformerlst:
                    conformer.write_to_file(outconformerfile, ID=cycle_number)
                for conformer in clashed_conformerlst:
                    conformer.write_to_file(outconformerfile, ID=-1)
                clashed_conformerlst = []
                for bestconformer in bestconformerlst:
                    bestconformer.write_to_file(outbestconformerfile, ID=cycle_number)

            if current_average is not None and (average_energy_bestconf * 0.9) <= current_average <= (average_energy_bestconf * 1.1):
                generation_convergence_counter += 1
            else:
                generation_convergence_counter = 0
            #generation_convergence_counter = 5 because that is the usual number of generations
            #before claiming convergence in a genetic algorithm.
            #A guaranteed first meteor strike.
            if generation_convergence_counter == self.convergence_counter_limit and not scorched_earth:
                # ~ print (f"\033[91m[DEBUG] SCORCHED EARTH >:)\033[0m")
                conformerlst = _meteor_strike(conformerlst)
                scorched_earth = True
                generation_convergence_counter = 0
            #And after it, a random one if conditions are met.
            elif generation_convergence_counter == self.convergence_counter_limit and scorched_earth and random.random() < 0.5:
                conformerlst = _meteor_strike(conformerlst)
                scorched_earth = True
                generation_convergence_counter = 0
            if generation_convergence_counter == self.convergence_counter_limit * 2:
                print("Energy convergence reached!")
                break
            current_average = average_energy_bestconf
            genetic = SetsGeneration(conformerlst, self.molecule)
            conformerlst = next(genetic)
            if not self.quiet:
                cycle_time= float(time.time() - startcycle_time)
                cycle_data = ['\033[94m', f"CYCLE: {cycle_number} ",
                                f"({cycle_time:.2f} seg)",
                                '\033[0m',
                                f"\nAverage energy of the conformers: {average_energy_bestconf:.4f} kcal/mol",
                                f"\nBest: {bestconf_energy:.4f} kcal/mol"]
                print("".join(cycle_data))
        bestconformers_data = (bestconformerlst, bestconf_energy, average_energy_bestconf)
        return bestconformers_data


################################
# BRUTE FORCE EXPLORATOR       #
################################

class BFExplorator:
    """
    This class is used to run the brute force optimizator.

    Attributes:
        molecule (MyMolecule): Molecule which confomers are of interest.
        calculator (Calculator): Calculator used to evaluate the energy of the confomers.
        quiet (bool): True to print to console, False to run quietly.
        max_final_conformers (int): Number of conformers to be generated.
        pool (Pool): Pool object to be used when running in parallel.
        range_generate (bool): True to generate a range (based on energy) of
                               confomers or False to generate a fixed final number of confomers.

    Methods:
        get_conformers
    """
    #Calculate permutations for n options on k sites
    comb_size = lambda self, n, k: pow(n, k)
    allowed_steps=[1, 2, 3, 4, 5, 6, 8, 9, 10, 12, 15, 18, 20, 24, 30, 36, 40, 45, 60, 72, 90, 120, 180, 360]

    def __init__(self, molecule, calculator, **kwargs):
        """
        Constructor for the GAExplorator class.

        Parameters:
            molecule (MyMolecule): Molecule which confomers are of interest.
            calculator (Calculator): Calculator used to evaluate the energy of the confomers.

        Arbitrary Keyword Args:
            quiet (bool): True to print to console, False to run quietly.
            max_gen (int): Number of generations to run the genetic algorithm.
            max_final_conformers (int): Number of conformers to be generated.
            members_number (int): Number of members to run the genetic algorithm.
            pool (Pool): Pool object to be used when running in parallel.
            energy_threshold (float): Energy threshold to consider two structures as different conformers.
            rmsd_threshold (float): RMSD difference to consider two structures as different conformers.
            range_generate (bool): True to generate a range (based on energy) of
                                   confomers or False to generate a fixed final
                                   number of confomers.
            energy_limit (float): Energy limit to set the range for range generation.
        """
        self.molecule = molecule
        self.calculator = calculator
        self.quiet = kwargs["quiet"]
        self.max_final_conformers = kwargs["max_final_conformers"]
        self.pool = kwargs["pool"]
        self.dihedral_step = kwargs["dihedral_step"]
        self.range_generate = kwargs["range_generate"]
        self.energy_threshold = kwargs["energy_threshold"]
        self.rmsd_threshold = kwargs["rmsd_threshold"]
        self.range_generate = kwargs["range_generate"]
        self.energy_limit = kwargs["energy_limit"]

        self.save_all = kwargs.get("save_all", False)

    def params_from_angles(self, angles):
        newparamlst = []
        for param, angle in zip(self.molecule.initial_paramlst, angles):
            newparam = param.newparam(angle)
            newparamlst.append(newparam)

        return newparamlst

    def get_conformers(self):
        if (360%self.dihedral_step != 0):
            print("Allowed step values:")
            print(*self.allowed_steps, sep=", ")
            exit()

        angles_tup = tuple(range(-180, 180, self.dihedral_step))
        ndihedrals_to_explore = len(self.molecule.initial_paramlst)

        print(f"{len(angles_tup)} angles for each {ndihedrals_to_explore} dihedrals to explore")
        total_conformers=int(self.comb_size(len(angles_tup), ndihedrals_to_explore))

        #### Ver si confirmar la generación de una guasada de confórmeros
        print(f"Maximum {total_conformers} conformers will be evaluated")

        # Generate a generator with tuples of size ndihedrals_toexplore and values in angles_to_explore,
        # with repetitions
        angles_to_explore = product(angles_tup, repeat=ndihedrals_to_explore)

        bestconformerlst = []
        conformerlst = []
        evaluated = 0
        generated=0

        if self.save_all:
            outbffilename = "./outbffile.log"
            outbffile=open(outbffilename, "w")

        for angles_tup in angles_to_explore:
            #Run the conformers in groups of 500 and clean the conformerlst.
            #Whether or not the list is cleaned, we add the next conformer.
            if len(conformerlst) >= 500:
                conformerlst = mainfunctions.calc_energy(conformerlst, self.molecule,
                                                        self.calculator, pool=self.pool)
                bestconformerlst = mainfunctions.energy_rmsd_filter(conformerlst+bestconformerlst,
                                                self.range_generate, self.max_final_conformers,
                                                self.energy_threshold, self.rmsd_threshold, self.energy_limit)
                if self.save_all:
                    for conformer in conformerlst:
                        conformer.write_to_file(outbffile)
                conformerlst = []

            tempconf = self.molecule.create_new_conformer(self.params_from_angles(angles_tup))
            generated +=1
            if mainfunctions.check_clashes(tempconf,self.molecule) is False:
                conformerlst.append(tempconf)
                evaluated += 1
            else:
                if self.save_all:
                    tempconf.energy = inf
                    conformerlst.append(tempconf)
                else:
                    del tempconf
        conformerlst = mainfunctions.calc_energy(conformerlst, self.molecule, self.calculator, pool=self.pool)
        bestconformerlst = mainfunctions.energy_rmsd_filter(conformerlst+bestconformerlst,
                                        self.range_generate, self.max_final_conformers,
                                        self.energy_threshold, self.rmsd_threshold, self.energy_limit)

        print(f"{generated} conformers were generated.")
        print(f"{evaluated} conformers were evaluated.")

        if self.save_all:
            for conformer in conformerlst:
                conformer.write_to_file(outbffile)
            outbffile.close()
            print(f"Saving all conformers to {outbffilename}.")

        bestconf_energy = sorted([bestconf.energy for bestconf in bestconformerlst])[0]
        average_energy_bestconf = sum([bestconf.energy for bestconf in bestconformerlst])/len(bestconformerlst)

        bestconformers_data = (bestconformerlst, bestconf_energy, average_energy_bestconf)
        return bestconformers_data

###################################
# RATIONAL BRUTE FORCE EXPLORATOR #
###################################

class RBFExplorator:
    """
    This class is used to run the brute force optimizator.

    Attributes:
        molecule (MyMolecule): Molecule which confomers are of interest.
        calculator (Calculator): Calculator used to evaluate the energy of the confomers.
        quiet (bool): True to print to console, False to run quietly.
        max_final_conformers (int): Number of conformers to be generated.
        pool (Pool): Pool object to be used when running in parallel.
        range_generate (bool): True to generate a range (based on energy) of
                               confomers or False to generate a fixed final number of confomers.

    Methods:
        get_conformers
    """
    comb_size = lambda self,n,r: factorial(n+r-1)/(factorial(r)*factorial(n-1))
    allowed_steps=[1, 2, 3, 4, 5, 6, 8, 9, 10, 12, 15, 18, 20, 24, 30, 36, 40, 45, 60, 72, 90, 120, 180, 360]

    def __init__(self, molecule, calculator, **kwargs):
        """
        Constructor for the RBFExplorator class.

        Parameters:
            molecule (MyMolecule): Molecule which confomers are of interest.
            calculator (Calculator): Calculator used to evaluate the energy of the confomers.

        Arbitrary Keyword Args:
            quiet (bool): True to print to console, False to run quietly.
            max_gen (int): Number of generations to run the genetic algorithm.
            max_final_conformers (int): Number of conformers to be generated.
            members_number (int): Number of members to run the genetic algorithm.
            pool (Pool): Pool object to be used when running in parallel.
            energy_threshold (float): Energy threshold to consider two structures as different conformers.
            rmsd_threshold (float): RMSD difference to consider two structures as different conformers.
            range_generate (bool): True to generate a range (based on energy) of
                                   confomers or False to generate a fixed final
                                   number of confomers.
            energy_limit (float): Energy limit to set the range for range generation.
        """
        self.molecule = molecule
        self.calculator = calculator
        self.quiet = kwargs["quiet"]
        self.max_final_conformers = kwargs["max_final_conformers"]
        self.pool = kwargs["pool"]
        self.dihedral_step = kwargs["dihedral_step"]
        self.range_generate = kwargs["range_generate"]
        self.energy_threshold = kwargs["energy_threshold"]
        self.rmsd_threshold = kwargs["rmsd_threshold"]
        self.range_generate = kwargs["range_generate"]
        self.energy_limit = kwargs["energy_limit"]

        self.save_all = kwargs.get("save_all", False)

    def params_from_angles(self, angles):
        newparamlst = []
        for param, angle in zip(self.molecule.initial_paramlst, angles):
            newparam = param.newparam(angle)
            newparamlst.append(newparam)

        return newparamlst

    def find_localmins(self, mylist):
        '''
        Returns the index where the local minima on a list are found
        '''
        localmins = []
        llen = len(mylist)
        if llen == 0:
            raise ValueError("Expected non-empty iterable")
        elif llen == 1:
            return [0]

        for i in range(llen):
            b = (i-1) % llen
            a = (i+1) % llen
            if mylist[i] <= mylist[b] and mylist[i] <= mylist[a]:
                localmins.append(i)
        if len(localmins) == 0:
            raise Warning('No minima found on the list, returning first element.')
            return [0]
        return localmins

    def get_conformers(self):
        if (360%self.dihedral_step != 0):
            print("Allowed step values:")
            print(*self.allowed_steps, sep=", ")
            exit()

        angles_tup = tuple(range(-180, 180, self.dihedral_step))
        ndihedrals_to_explore = len(self.molecule.initial_paramlst)
        initial_angles = [param for param in self.molecule.initial_paramlst]

        print(f"{len(angles_tup)} angles, {ndihedrals_to_explore} dihedrals")
        total_conformers = len(angles_tup) * ndihedrals_to_explore

        print(f"I will generate {total_conformers} conformers for RBF.")

        if self.save_all:
            clashed_conformerlst = []
            outconformerfilename = f"./outrbffile_{self.molecule.name}.log"
            outconformerfile = open(outconformerfilename, "w")

        bestconformerlst = []

        bestangles=[]
        for i in range(ndihedrals_to_explore):
            conformerlst = []
            for angle in angles_tup:
                angleset = initial_angles.copy()
                angleset[i] = angle
                tempconf = self.molecule.create_new_conformer(self.params_from_angles(angleset))
                if mainfunctions.check_clashes(tempconf,self.molecule) is False:
                    conformerlst.append(tempconf)
                else:
                    if self.save_all:
                        tempconf.energy = inf
                        conformerlst.append(tempconf)
                    del tempconf
            conformerlst = mainfunctions.calc_energy(conformerlst, self.molecule, self.calculator, pool=self.pool)
            energylst = [conf.energy for conf in conformerlst]
            bestangles.append([angles_tup[myindex] for myindex in self.find_localmins(energylst)])
            print(f"Best angles for dihedral {i+1}: {bestangles[-1]}")
            
            if self.save_all:
                for conformer in conformerlst:
                    conformer.write_to_file(outconformerfile, ID=i)
            #Save the best, as we already calculated their energy
            bestconformerlst = mainfunctions.energy_rmsd_filter(conformerlst+bestconformerlst,
                                                                self.range_generate, self.max_final_conformers,
                                                                self.energy_threshold, self.rmsd_threshold, self.energy_limit)
                                                            
        #Make combinations: product([A, B, C], [D. E]) = (AD, AE, BD, BE, CD, CE)
        angles_to_test = product(*bestangles)
        conformerlst = []
        for angleset in angles_to_test:
            tempconf = self.molecule.create_new_conformer(self.params_from_angles(angleset))
            if mainfunctions.check_clashes(tempconf,self.molecule) is False:
                conformerlst.append(tempconf)
            else:
                if self.save_all:
                    tempconf.energy = inf
                    conformerlst.append(tempconf)
                del tempconf
        conformerlst = mainfunctions.calc_energy(conformerlst, self.molecule, self.calculator, pool=self.pool)
        
        if self.save_all:
            for conformer in conformerlst:
                conformer.write_to_file(outconformerfile, ID=ndihedrals_to_explore)

        bestconformerlst = mainfunctions.energy_rmsd_filter(conformerlst+bestconformerlst,
                                                            self.range_generate, self.max_final_conformers,
                                                            self.energy_threshold, self.rmsd_threshold, self.energy_limit)
        bestconf_energy = sorted([bestconf.energy for bestconf in bestconformerlst])[0]
        average_energy_bestconf = sum([bestconf.energy for bestconf in bestconformerlst])/len(bestconformerlst)

        bestconformers_data = (bestconformerlst, bestconf_energy, average_energy_bestconf)
        return bestconformers_data


#############################
# PARTICLE SWARM EXPLORATOR #
#############################

class PSOParticle:
    """
    This class holds a particle info used in the Particle Swarm Optimization algorithm.

    The particle behaves as an insect. its trajectory is affected by the least energy position
    found by the particle, and the least energy position ever found in the run. The relative weight
    of each factor is adjusted with the c_mybest and c_globalbest variables. c_inertial set the
    relative weight for the current trajectory. All the c's are class-wide variables.

    Attributes:
        params: [objects.Parameters]
            List of Parameters whose values epresent the current position of the particle in space. As
            this program is a conformational search tool, the params generrally represent each dihedral
            angle present in the molecule.
        bestparams: [object.Parameters]
            List of parameters where the particle has been and found the lower energy in it's path. Adds
            a velocity in this direction, with weight affected by c_mybest.
        energy_func: function
            Function used to get the particle's energy. Sould take the particle itself as an argument.
        fitness: float
            Current energy (or fitness) evaluated in the last movement.
        vel: [objects.Parameters]
            Curent velocity, uptaded in each call to move() before movement.

    Methods:

        set_velocity_weights(cls, c_inertial=None, c_local_best=None, c_global_best=None):
            Set the weights used when combining the velocity components in each simulation step.
        move(self, global_best_param): move the particle a step in the simulation.
        circ_dif(a, b):
            calculate the circular difference between two values. for example, the difference between
            170 degrees and -170 degrees is -20 degrees.
        lin_dif(a, b):
            Calculate the linear difference betewwn values.
        clamp(num, min_value, max_value):
            Restringe the number num to the max and min values.
        wrap(num, min_value, max_value):
            If the number num is out of the (min_value, max_value) range, wrap its value around and return it.
            For example, wrap(20, 5, 17) returns 7.
        write_to_file(self, outfile, index=0):
            Write to the outfile a line containing the dihedrals' values, the particle fitness and a classifying index
    """

    # diose: 2.6 8.6 7.6
    # diphe 3.6 0.88 1.82
    c_inertial   = 2.6
    c_mybest     = 8.6
    c_globalbest = 7.6
    
    #New, according to PSO-ISTE by Maurice Clerc (2006)
    c_1 = 0.689343
    c_max = 1.42694

    _part_ID = 0

    global_best_param = None

    # Get the angle difference when unit is angle in -180, 180 range
    circ_dif = lambda a, b: b - a - int((b-a)/abs(b-a)) * 180 if abs(b-a) > 180 else b-a
    lin_dif = lambda a, b: b - a
    clamp = lambda self, num, min_value, max_value: max(min(num, max_value), min_value)
    wrap = lambda self, num, min_value, max_value: (num - min_value) % (max_value - min_value + 1) + min_value

    #Map each difference function to each parameter type
    difdict = {"D": circ_dif, "A": circ_dif, "B": lin_dif}

    def set_velocity_weights(c_inertial, c_local_best, c_global_best):
        '''
        Set the weights for each velocity used to calculate the new velocity in each step.

        Parameters
            c_inertial (float):
                weight for the particle's current velocity.
            c_local_best (float):
                weight for the velocity in the particle's best found position.
            c_global_best (float):
                weight for the velocity in the all-time all-particles best found position.
        '''
        PSOParticle.c_inertial   = c_inertial
        PSOParticle.c_mybest     = c_local_best
        PSOParticle.c_globalbest = c_global_best

    def set_velocity_weights(c_1, c_max):
        '''
        Set the weights for each velocity used to calculate the new velocity in each step.

        Parameters
            c_inertial (float):
                weight for the particle's current velocity.
            c_local_best (float):
                weight for the velocity in the particle's best found position.
            c_global_best (float):
                weight for the velocity in the all-time all-particles best found position.
        '''
        PSOParticle.c_1   = c_1
        PSOParticle.c_max = c_max

    def __init__(self, start_params, energy_func, maxvel=30):
        '''
        Create and initialize a PSO particle.

        Parameters:
            start_params: [objects.Parameter]
                List of starting parameters (molecule dihedrals in this case) for this particle.
            energy_func: function
                Function used to get the particle's energy. Takes as an argument the particle itself.

        '''
        self.params = start_params
        self.bestparams = start_params
        self.energy_func = energy_func
        self.maxvel = maxvel

        self.fitness = self.energy_func(self)
        self.bestfitness = self.fitness
        self.vel = [random.uniform(-1, 1) * self.maxvel for param in self.params]
        self.ID = PSOParticle._part_ID
        PSOParticle._part_ID += 1

        # ~ print(f"initial_v {self.vel}")

    def __str__(self):
        '''
        Create str with each parameter's value and the current fitness. Similar to the MyConformer method.
        '''
        return "".join([f"{param.value: >7.2f} " for param in self.params]) + str(self.fitness)

    def velocity_to(self, target_params):
        '''
        Return the velocity to the target params as the distance multiplied by the c factor.
        '''
        return [self.difdict[pos.paramtype](pos.value, target.value) for target, pos in zip(target_params, self.params)]
        

    def move(self, informants=None):
        '''
        Move the particle a step in the corresponding direction.
    
        First, update the velocity according to all the existing particles' current velocities. Then,
        move in the new velocity's direction (with the velocity's modulus).
        '''        
        #The new velocity is built as:
        #A sum of constant parameters in the direction of: the current velocity, this particles best, and the global best
        #Testing the ones from the book
        # ~ currentvel = [self.c_inertial * vel for vel in self.vel]
        # ~ tomybestvel =  [self.c_mybest * vel for vel in self.velocity_to(self.bestparams)]
        # ~ tobestbestvel = [self.c_globalbest * vel for vel in self.velocity_to(self.global_best_param)]


        if informants is not None or len(informants) != 0:
            # The particle always has access to its own information
            informants_best_fitness = self.bestfitness
            informants_best_param = self.bestparams
            for informant in informants:
                if informant.fitness < informants_best_fitness:
                    informants_best_fitness = informant.fitness
                    informants_best_param   = informant.params

        currentvel    = [self.c_1 * vel for vel in self.vel]
        tomybestvel   = [self.c_max * random.random() * vel for vel in self.velocity_to(self.bestparams)]
        # ~ tobestbestvel = [self.c_max * random.random() * vel for vel in self.velocity_to(self.global_best_param)]
        tobestbestvel = [self.c_max * random.random() * vel for vel in self.velocity_to(informants_best_param)]

        newvel = [a + b + c for a, b, c in zip(currentvel, tomybestvel, tobestbestvel)]

        # Not needed for c1 < 1
        # ~ #Scale the new velocity if it exceeds tha maximum set
        # ~ vel_abs = sqrt(sum([comp**2 for comp in newvel]))
        # ~ if vel_abs >= self.maxvel:
            # ~ factor = self.maxvel / vel_abs
            # ~ newvel = [v * factor for v in newvel]

        #Set the velocity, move the particle, and constrain the resulting position accordong to the parameter type
        self.vel = newvel
        newparams = [param.value + vel for param, vel in zip(self.params, newvel)]

        for param, newparam in zip(self.params, newparams):
            if param.paramtype in ("D", "B"):
                param.value = self.wrap(newparam, param.min_value, param.max_value)
            elif param.paramtype == "A":
                param.value = self.clamp(newparam, param.min_value, param.max_value)

    def update_energy(self, energy):
        '''
        Update the particle's energy after moving. If its a new low, replace the best parameters and energy found.
        '''
        self.fitness = energy
        if self.fitness < self.bestfitness:
            self.bestfitness = self.fitness
            self.bestparams = [copy.copy(param) for param in self.params]
            return True

        return False

    def write_to_file(self, outfile, ID=0):
        '''
        Write the particle to a file.
        
        The format is 7-character long columns with each parameter's values, then the energy and an ID to classify the particle.
        '''
        outfile.write(str(self) + f" {ID}\n")

class PSOExplorator:
    """
    Implementation of a particle swarm optimizer.

    A Particle Swarm Optimizer (PSO) works by using a group of particles to explore the fitness landscape.
    Each Particle starts at a random location and asseses its fitness. Then, the particles move in a direction
    combining the best known fitness found, its own best explored fitness and the currend direction. Because
    the particles are not affected by the fitness landscape, they behave as looking it through a 'bird's eye'.
    This pattern is characteristic of insects like bees, and so the name.

    Attributes:
        molecule (objects.MyMolecule):
            Molecule for wich to explore the conformational energy landscape.
        calculator (calculator):
            Calculator used to evaluate the confomational energy.
        pool (multiprocessing.Pool):
            Pool object to run the energy evaluation in parallell.
        particles (list, PSOParticle):
            List of the particles in the swarm.
        bestparams (list, objects.Parameter):
            List with the best parameters found overall in the run.
        gbestfirness (float):
            The best fitness found overall in the run.

    """

    def __init__(self, molecule, calculator, **kwargs):
        """
        Constructor for the PSOExplorator class.

        Parameters:
            molecule (MyMolecule): Molecule which confomers are of interest.
            calculator (Calculator): Calculator used to evaluate the energy of the confomers.

        Arbitrary Keyword Args:
            quiet (bool): True to print to console, False to run quietly.
            max_gen (int): Number of generations to run the genetic algorithm.
            max_final_conformers (int): Number of conformers to be generated.
            members_number (int): Number of members to run the genetic algorithm.
            pool (Pool): Pool object to be used when running in parallel.
            energy_threshold (float): Energy threshold to consider two structures as different conformers.
            rmsd_threshold (float): RMSD difference to consider two structures as different conformers.
            range_generate (bool): True to generate a range (based on energy) of
                                   confomers or False to generate a fixed final
                                   number of confomers.
            energy_limit (float): Energy limit to set the range for range generation.
        """
        self.molecule = molecule
        self.calculator = calculator
        self.quiet = kwargs["quiet"]
        self.max_gen = kwargs["max_gen"]
        self.max_final_conformers = kwargs["max_final_conformers"]
        self.members_number = kwargs["members_number"]
        self.pool = kwargs["pool"]
        self.energy_threshold = kwargs["energy_threshold"]
        self.rmsd_threshold = kwargs["rmsd_threshold"]
        self.range_generate = kwargs["range_generate"]
        self.energy_limit = kwargs["energy_limit"]
        self.nparticles = kwargs["nparticles"]
        self.pso_steps = kwargs["pso_steps"]

        self.particles = []

        bestparams = None
        gbestfitness = None

        self.ninformants = kwargs.get("ninformants", 3)
        self.save_all = kwargs.get("save_all", False)


    def update_globalbest(self):
        """
        Update the lowest energy and parameters found.

        Check if any of the particles' energy is lower than the lowerst found, and if so,
        update the lowest energy and corresponding parameters found.
        """
        # Puts the best energy and parameters of the particles as the global best
        bestparticle = sorted(self.particles.copy(), key=lambda particle: particle.fitness)[0]
        if bestparticle.fitness < self.gbestfitness:
            self.gbestfitness = bestparticle.fitness
            #For params, as it is a Parameters object list, we need to do a deepcopy
            #Else, it keeps following the last best particle's parameters
            self.gbestparams = bestparticle.params.copy()
            PSOParticle.global_best_param = [copy.copy(param) for param in self.gbestparams]

    def particle_energy(self, particle):
        '''
        Get a particle's energy from its conformer.
        '''
        return self.params_energy(particle.params)
        
    def params_energy(self, params):
        '''
        Get a particle's energy from its params.
        '''
        new_conformer = self.molecule.create_new_conformer(params.copy())
        return self.calculator.get_energy(self.molecule.atoms_properties["symbol"],
                                          new_conformer.atoms_coords)

    def get_conformers(self):
        """
        Return a list of the best generated conformers.

        Returns:
            bestconfomers_data (tuple: list, float, float):
                Tuple with the best conformers' data obtained by the genetic algorithm.
                Found MyConformers, energy of the best and average energy of the final assembly.
        """
        #Initialize variables
        conformerlst = []
        bestconformerlst = []

        if self.save_all:
            outpsofilename = "./outpsofile.log"
            outpsofile=open(outpsofilename, "w")
            outbestconformerfilename = "./outbestpsofile.log"
            outbestconformerfile = open(outbestconformerfilename, "w")

        #Start with random conformers
        while len(self.particles) < self.nparticles:
            #Use the initial conformer to generate a new member
            new_paramlst = mainfunctions.generate_values(self.molecule.initial_paramlst)
            new_conformer = self.molecule.create_new_conformer(new_paramlst)
            if mainfunctions.check_clashes(new_conformer,self.molecule) is False:
                new_part = PSOParticle(new_paramlst, self.particle_energy)
                self.particles.append(new_part)
            else:
                if self.save_all:
                    new_conformer.energy = inf
                    new_conformer.write_to_file(outpsofile, -1)
                del new_paramlst
                del new_conformer

        self.gbestfitness = inf

        self.update_globalbest()
        conformerlst = [self.molecule.create_new_conformer(particle.params.copy(), particle.fitness) for particle in self.particles]
        bestconformerlst = mainfunctions.energy_rmsd_filter(conformerlst + bestconformerlst,
                                                            self.range_generate, self.max_final_conformers,
                                                            self.energy_threshold, self.rmsd_threshold, self.energy_limit)

        continuebest = 0

        for npaso in range(self.pso_steps):
            #Assign to each particle a set of random informants from where it gets
            #the best curent position.
            for particle in self.particles:
                candidatos = self.particles.copy()
                candidatos.remove(particle)
                particle.move(informants=random.sample(candidatos, self.ninformants))

            #For energy updating, we need to create a tomporary list of conformers, calculate their
            #energy, and map it to each particle's fitness
            #When using the implemented funcions (self.particle_energy) with a starmap, the problem is that it needs a 
            #calculator, which is in the explorator itself, so it need the explorator, and in the explorator is the pool
            #and since it cannot pickle the pool itself, it raises an error
            #TODO: Replace with a better version?
            conformerlst = [self.molecule.create_new_conformer(deepcopy(particle.params)) for particle in self.particles]
            for conformer in conformerlst:
                if mainfunctions.check_clashes(conformer):
                    conformer.energy = inf
            energy_conformers = mainfunctions.calc_energy(conformerlst, self.molecule, self.calculator, pool=self.pool)
            
            e_index = 0
            for conformer in conformerlst:
                if conformer.energy == None:
                    conformer.energy = energy_conformers[e_index].energy
                    e_index += 1

            reset = False
            for part, conf in zip(self.particles, conformerlst):
                if part.update_energy(conf.energy):
                    reset = True
            if reset:
                continuebest = 0
            else:
                continuebest += 1

            if self.save_all:
                for part, conf in zip(self.particles, conformerlst):
                    conf.write_to_file(outpsofile, part.ID)

            self.update_globalbest()
            bestconformerlst = mainfunctions.energy_rmsd_filter(conformerlst + bestconformerlst,
                                                                self.range_generate, self.max_final_conformers,
                                                                self.energy_threshold, self.rmsd_threshold, self.energy_limit)

            # ~ bestconf_energy = sorted([bestconf.energy for bestconf in bestconformerlst])[0]
            # ~ average_energy_bestconf = sum([bestconf.energy for bestconf in bestconformerlst])/len(bestconformerlst)

            if continuebest >= 10:
                if not self.quiet:
                    print(f"Energy convergence reached after {npaso+1} steps!")
                break

        #Put it here for completeness, but i dont know it it would be useful another quantity
        average_energy_bestconf = sum([bestconf.energy for bestconf in bestconformerlst]) / len(bestconformerlst)
        bestconf_energy = self.gbestfitness

        print(f"\nAverage energy of the conformers: {average_energy_bestconf:.4f} kcal/mol")
        print(f"\nBest: {bestconf_energy:.4f} kcal/mol")

        if self.save_all:
            for conformer in bestconformerlst:
                conformer.write_to_file(outbestconformerfile, ID=0)
            outpsofile.close()

        bestconformers_data = (bestconformerlst, bestconf_energy, average_energy_bestconf)
        return bestconformers_data
