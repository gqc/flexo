"""Calcultors documentation

This module holds all the de facto FLEXO calculators.

Conventions
-----------
All calculators some common methods. For example:

- get_energy method, which gives the single point energy of a molecular conformation.
- get_opt method, which gives the optimized coordinates and energy of the molecule conformer.

If you want to build your own calculator, just add the class here and import it in flexo.py.
For compatibility with the explorators, your calculator must have a get_energy() method that returns a float (the energy (?)
and a get_opt() method that optimizes the molecular geometry and returns an array of optimized coordinates and the corresponding energy. You should inherit from the Calculator class.
"""
import os
import re
import shutil
import subprocess
import tempfile

import numpy as np
from flexo.modules.cfunctions import obcalc

class Calculator:
    """
    Template class for calculator building.
    Currently implemented calculators in FLEXO are:

    - MMCalculator: Uses molecular mechanics force fields implemented in openbabel: MMFF94, GAFF, Ghemical, UFF.
    - MopacCalculator: Uses MOPAC as a backend for semi-empirical calculations.
    - DFTBCalculator: Uses DFTB as a backend to make semi-empirical calculations.
    - OrcaCalculator: Ues ORCA as a backend to make DFT ab-initio calculations.

    """
    def __init__(self, force_field, molecule_path):
        pass

    def get_energy(self, symbol_list, atoms_coords, index=None, *args):
        pass

    def get_opt(self, symbol_list, atoms_coords, index, molecule_name, *args):
        pass

class MMCalculator:
    """
    This class conects with openbabel to make the energy calculation and geometry optimization.
    """
    def __init__(self, force_field, molecule_path):
        """
        Constructor for the PybelCalculator class.

        Parameters
        ----------
        molecule_path : str
            Absolute path to the .mol2 file containing the molecule of interest.
        force_field : str
            Force field name.
        """
        self.force_field = force_field
        self.molpath = f"{os.getcwd()}/{molecule_path}"
        if not (obcalc.setupCalc(molecule_path, force_field)):
            raise RuntimeError("Error when setting up openbabel molecule or force field")

    def get_energy(self, symbol_list, atoms_coords, index=None, *args):
        """
        Return the energy of a conformer calculated using Molecular Mechanics as implented in Openbabel.

        Parameters
        ----------
        symbol_list : list(str)
            List of atomic symbols. Unused here, but for completion.
        atoms_coords : np.ndarray(float)
            x, y, z coordinates of the atoms in the conformer.
        index : int
            Index number of the confomer.
        atom_type_list : list(str)
            List containing the atoms' type.
        atom_charge_list : list(int)
            List containing the atoms' charges
        original_bond_list : list([int, int, int])
            List containing, in order: Index of atom1, index of atom2, bond order between them.
        molecule_name : str
            Name of the molecule.

        Returns
        -------
        energy : float
            Energy calculated.

        Raises
        ------
        RuntimeError
            If it could not setup OB calculation on c++.
        """
        atoms_coords = atoms_coords
        energy = float(obcalc.getEnergy(atoms_coords))
        if self.force_field != "MMFF94":
            energy /= 4.184 #kJ > kcal
        return energy

    def get_opt(self, symbol_list, atoms_coords, index, molecule_name, *args):
        """
        Locally optimize a conformer in 500 steps using Molecular Mechanics as implented in openbabel.

        Parameters
        ----------
        symbol_list : list(str)
            List of atomic symbols.
        atoms_coords : np.array(float)
            x, y, z coordinates of the atoms in the conformer.
        index : int
            Index number of the confomer. Just as a local ID.
        molecule_name :str
            Name of the parental molecule.

        Returns
        -------
        coords : np.ndarray
            Nx3 array with the conformer's optimized coordinates.
        energy : float
            Energy of the optimized conformer.
        """
        optcoords=obcalc.optimize(atoms_coords, 500, self.molpath)
        optenergy=float(obcalc.getEnergy(optcoords))
        if self.force_field != "MMFF94":
            optenergy /= 4.184 #kJ > kcal
        return np.array(optcoords), optenergy



class PybelCalculator:
    """
    This class conects with openbabel to make the energy calculation and geometry optimization.
    """

    def __init__(self, force_field, molecule_path):
        """
        Constructor for the PybelCalculator class.

        Parameters
        ----------
            molecule_path (str): Absolute path to the .mol2 file containing the molecule of interest.
            force_field (str): Force field name.
        """
        from importlib import import_module
        try:
            openbabelimp = import_module('openbabel.openbabel')
        except ImportError:
            openbabelimp = import_module('openbabel')
        try:
            pybelimp = import_module('openbabel.pybel')
        except ImportError:
            pybelimp = import_module('pybel')

        self.readstring = pybelimp.readstring
        self.OBForceField = openbabelimp.OBForceField

        self.molecule_path = molecule_path
        self.force_field = force_field
        self._molecule_rti = self._get_molecule_rti()


    def _get_molecule_rti(self):
        MOLECULE_RTI_RE = re.compile(r'(?s)(?=\@\<TRIPOS\>MOLECULE\n).*(\@\<TRIPOS\>ATOM)')
        with open(self.molecule_path, "r") as molecule_file:
            molecule_file_readed = molecule_file.read()
            found = re.search(MOLECULE_RTI_RE, molecule_file_readed)
            return found.group(0)+"\n"

    def get_energy(self, symbol_list, atoms_coords, index=None, *args):
        """
        Return the energy of a conformer calculated using Molecular Mechanics as implented in Openbabel.

        Parameters
        ----------
        symbol_list : list(str)
            List of atomic symbols.
        coords_list : np.array(float)
            x, y, z coordinates of the atoms in the conformer.
        index : int
            Index number of the confomer.

        Arbitrary Args:
            atom_type_list (list, str)
            atom_charge_list (list, int)
            original_bond_list (list, [int, int, int]): [Index of atom1, index of atom2, bond order between them]
            molecule_name (str): Name of the molecule.

        Returns
        -------
        energy : float
            Energy calculated.

        Raises
        ------
        RuntimeError
            If it could not find or setup forcefield.
        """
        atom_type_list = args[0]
        atom_charge_list = args[1]
        original_bond_list = args[2]
        molecule_name = args[3]
        mol2_coords_and_props = []
        mol2_coords_and_props.append(self._molecule_rti)

        for index, (symbol, coord, atom_type, atom_charge) in enumerate(zip(symbol_list, atoms_coords, atom_type_list, atom_charge_list)):
            coords_str = " ".join([str(line) for line in coord])
            mol2_coords_and_props.append(str(index+1))
            mol2_coords_and_props.append(str(symbol))
            mol2_coords_and_props.append(str(coords_str))
            mol2_coords_and_props.append(str(atom_type))
            mol2_coords_and_props.append("1")
            mol2_coords_and_props.append(str(molecule_name))
            mol2_coords_and_props.append(str(atom_charge)+"\n")

        mol2_coords_and_props_str = " ".join(mol2_coords_and_props)+"@<TRIPOS>BOND\n"
        bond_list = []
        for index, bond in enumerate(original_bond_list):
            bond_list.append(str(index+1))
            bond_list.append(str(bond[0]))
            bond_list.append(str(bond[1]))
            bond_list.append(str(bond[2])+"\n")
        bond_list_str = " ".join(bond_list)
        mol2_str = mol2_coords_and_props_str+bond_list_str
        mymol = self.readstring("mol2", mol2_str)
        ff = self.OBForceField.FindForceField(self.force_field)
        if ff is None:
            raise RuntimeError (f"Could not find forcefield {self.force_field} using OpenBabel!")
        if ff.Setup(mymol.OBMol) is None:
            raise RuntimeError (f"Could not setup forcefield {self.force_field} using OpenBabel!")
        energy = float(ff.Energy())
        if self.force_field != "MMFF94":
            energy /= 4.184 #kJ > kcal
        return energy

    def get_opt(self, symbol_list, atoms_coords, index, molecule_name, *args):
        """
        Locally optimize a conformer in 500 steps using Molecular Mechanics as implented in openbabel.

        Parameters
        ----------
        symbol_list : list(str)
            List of atomic symbols.
        atoms_coords : np.array(float)
            x, y, z coordinates of the atoms in the conformer.
        index : int
            Index number of the confomer. Just as a local ID.
        molecule_name :str
            Name of the parental molecule.

        Returns
        -------
        coords : np.ndarray
            Nx3 array with the conformer's optimized coordinates.
        energy : float
            Energy of the optimized conformer.
        """
        xyzstr = f"{len(symbol_list)}\n\n"
        for symbol, coord in zip(symbol_list, atoms_coords):
            atoms_str = " ".join([str(line) for line in coord])
            xyzstr += f"{symbol} {atoms_str}\n"
        mymol = self.readstring("xyz", xyzstr)
        mymol.localopt(forcefield=self.force_field, steps=500)
        os.chdir(f"{molecule_name}_omm_conformers_opt")
        file_name = f"{molecule_name}_conformer{index}_opt.xyz"
        mymol.write(format='xyz', filename=file_name, overwrite=True)
        os.chdir("../")

        optcoords = [list(atom.coords) for atom in mymol.atoms]
        #TODO: ver como devolver energia
        return np.array(optcoords), 0


class MopacCalculator:
    """
    This class conects with mopac to make the energy calculation and geometry optimization.

    Attributes
    ----------
    se_method : str
        Semi-Empirical method to be used.
    charge : int
        Total charge of the molecule.

    Methods
    -------
        get_energy
        get_opt
        _mopac_makeinput
        _mopac_outputread
    """

    def __init__(self, se_method, charge, mopac_path, mopac_extra_keywords=""):
        """
        Constructor for the MopacCalculator class.

        Parameters
        ----------
        se_method : str
            Semi-Empirical method to be used.
        charge : int
            Total charge of the molecule.
        mopac_path : str
            Path to MOPAC's binary.
        """
        self.se_method = se_method
        self.charge = charge
        self.mopac_keywords = f"{self.se_method.upper()} CHARGE={self.charge} " + mopac_extra_keywords
        self.filename = "temporal_mopac_file"
        self.mopac_path = mopac_path


    def _mopac_makeinput(self, symbol_list, coords_list, energy_or_opt, index=None, molecule_name=None):
        """
        Make and wite to file a  MOPAC input file.

        Parameters
        ----------
        symbol_list : list(str)
            List of atomic symbols.
        coords_list : np.array(float)
            Array with the x, y, z coordinates of the atoms in the conformer.
        energy_or_opt : str
            Choose energy calculation 'energy' or geometry optimization 'opt'.
        index : int
            Index number of the confomer.
        molecule_name : str
            Name of the parental molecule.
        """
        inputfilename = self.filename+".mop"
        if energy_or_opt == "energy":
            with open(inputfilename, "w") as inputfile:
                print("{0}\n\n".format(self.mopac_keywords+" 1SCF"), file=inputfile)
                for symbol, coord in zip(symbol_list, coords_list):
                    print(f"{symbol}  {coord[0]} 1 {coord[1]} 1 {coord[2]} 1\n", file=inputfile, sep="", end="")
        elif energy_or_opt == "opt":
            input_opt = molecule_name+"_conformer"+str(index)+"_opt"+".mop"
            with open(input_opt, "w") as inputfile:
                print("{0}\n\n".format(self.mopac_keywords+ " OPT"), file=inputfile)
                for symbol, coord in zip(symbol_list, coords_list):
                    print(f"{symbol}  {coord[0]} 1 {coord[1]} 1 {coord[2]} 1\n", file=inputfile, sep="", end="")


    def get_energy(self, symbol_list, coords_list, index=None, *args):
        """
        Return the energy of a conformer calculated using the stipulated Semi-Empirical method as implented in MOPAC.

        Parameters
        ----------
        symbol_list : list(str)
            List of atomic symbols.
        coords_list : np.array(float)
            x, y, z coordinates of the atoms in the conformer.
        index : int
            Index number of the confomer.
        args : dict
            Just a place holder so we can call get_energy() in the same way for every Calculator.

        Returns
        -------
        float
            Energy calculated.

        """
        basefolder = os.getcwd()
        carpeta = tempfile.TemporaryDirectory(prefix="tmpmopac_")
        os.chdir(carpeta.name)
        self._mopac_makeinput(symbol_list, coords_list, "energy", index)
        run_file = f"{self.filename}.mop"
        subprocess.run([self.mopac_path, run_file], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        energy = self._mopac_outputread(f"{self.filename}.out")
        carpeta.cleanup()
        os.chdir(basefolder)
        return energy


    def get_opt(self, symbol_list, coords_list, index, molecule_name, *args):
        """
        Optimize the geometry of a conformer the stipulated Semi-Empirical method as implented in MOPAC.

        Parameters
        ----------
        symbol_list : list(str)
            List of atomic symbols.
        atoms_coords : np.array(float)
            x, y, z coordinates of the atoms in the conformer.
        index : int
            Index number of the confomer. Just as a local ID.
        molecule_name :str
            Name of the parental molecule.

        Returns
        -------
        coords : np.ndarray
            Nx3 array with the conformer's optimized coordinates.
        energy : float
            Energy of the optimized conformer.
        """
        # TODO: use folder optimization name as parameter
        os.chdir(f"{molecule_name}_se_conformers_opt")
        os.mkdir(f"{molecule_name}_conformer{str(index)}_opt")
        os.chdir(f"{molecule_name}_conformer{str(index)}_opt")
        self._mopac_makeinput(symbol_list, coords_list, "opt", index, molecule_name)
        run_file = f"{molecule_name}_conformer{str(index)}_opt"
        subprocess.run([self.mopac_path, f"{run_file}.mop"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        energy, optcoords = self._mopac_outputread(f"{run_file}.out", getcoords=True)
        os.chdir("../../")
        return np.array(optcoords), energy

    def _mopac_outputread(self, outputfile, getcoords=False):
        """
        Read the output of a MOPAC run.

        Parameters
        ----------
        ourputfile : str
            Orca file with output.
        getcoords : bool
            Wether to get or not the coordinates from the output file.

        Returns
        -------
        heatformation : float
            Calculated heat of formation.

        Raises
        ------
            RuntimeError: If something went bad when reading the MOPAC output.
        """
        heatformation = None
        KEYFHOF = "FINAL HEAT OF FORMATION"
        outputstream=open(outputfile, "r")
        for line in outputstream:
            if KEYFHOF in line:
                linediv = line.split()
                heatformation = float(linediv[5]) #Extract FINAL HEAT OF FORMATION in KCAL/MOL
                break
        if getcoords:
            coordslst=[]
            #Final coords are after FINAL HEAT OF FORMATION
            for line in outputstream:
                if "CARTESIAN COORDINATES" in line:
                    break
            #Blank line before coords
            outputstream.readline()
            for line in outputstream:
                if line != "\n":
                    coordslst.append([float(coord) for coord in line.strip().split()[2:]])
                else:
                    break

            if heatformation is not None:
                return heatformation, coordslst

        if heatformation is not None:
            return heatformation
        else:
            shutil.copyfile(outputfile, f"problematic_{outputfile}")
            # If there is a problem, maybe it was a clash.
            raise RuntimeError (f"MOPAC problem: Check problematic_{outputfile}!")


class DFTBCalculator:
    """
    This class conects with DFTB to make the energy calculation.

    Attributes:
        template (str): TODO

    Methods:
        get_energy
         _e_dftb
    """

    def __init__(self, filename="dftb_in.hsd"):
        """
        TODO DOCS
        """
        self.template = filename
        os.environ["OMP_NUM_THREADS"] = "1"


    def _e_dftb(self, archivo):
        """
        Read the output of a DFTB run.

        Returns:
            total_energy (float): Calculated energy.

        Raises:
            RuntimeError: If something went bad when reading the DFTB output.
        """
        for line in open(archivo, "r"):
            if "Total energy:" in line:
                e = line.split()
            else:
                shutil.copyfile("detailed.out", "problematic_calculation.out")
                # If there is a problem, maybe it was a clash.
                raise RuntimeError ("ERROR: Check problematic_calculation.out!")
        total_energy = float(e[2])
        return total_energy

    def get_energy(self, symbol_list, coords_list, index=None, *args):
        """
        Return the energy of a conformer calculated using DFTB.

        Parameters:
            symbol_list (list, str): List of atomic symbols.
            coords_list (np.array, float): x, y, z coordinates of the atoms in the conformer.
            index (int): Index number of the confomer.

        Returns:
            energy (float): Energy calculated.

        Arbitrary Args:
            Just a place holder so we can call get_energy() in the same way for every Calculator.
        """
        carpeta = f"molecule_{index}"
        os.chdir("dftb_tmp")
        try:
            os.mkdir(carpeta)
        except FileExistsError:
            pass
        os.chdir(carpeta)
        xyz = open("mol.xyz","w")
        xyz.write(str(len(coords_list))+"\n\n")
        for symbol, coord in zip(symbol_list, coords_list):
            print(f"{symbol}  {str(coord[0])}  {str(coord[1])}  {str(coord[2])} \n", file=xyz, sep="", end="")
        xyz.close()
        os.system("xyz2gen  mol.xyz")
        os.system(f"cp ../../{self.template} .")
        os.system("dftb+ > log")
        energy = self._e_dftb("detailed.out")
        os.chdir("../")
        shutil.rmtree(carpeta)
        os.chdir("../")

        return energy
