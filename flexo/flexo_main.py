#!/usr/bin/env python3
"""FLEXO main file.
"""
__all__ = ["MyConfigReader", "main", "Bcolors"]

import argparse
import configparser
import datetime
from multiprocessing import Pool
import os
import shutil
import sys
import inspect

#SCRIPT_DIR = (os.path.realpath(os.path.dirname(inspect.getfile(inspect.currentframe()))))
#sys.path.append(os.path.dirname(SCRIPT_DIR))

from flexo.modules.calculators import MopacCalculator, PybelCalculator, DFTBCalculator, MMCalculator
from flexo.modules.explorators import GAExplorator as ga
from flexo.modules.explorators import BFExplorator as bf
from flexo.modules.explorators import RBFExplorator as rbf
from flexo.modules.explorators import PSOExplorator as pso
from flexo.modules import mainfunctions
from flexo.modules import objects


class Bcolors:
    red_fail = '\033[91m'
    green_ok = '\033[92m'
    warning_yellow = '\033[93m'
    running_blue = '\033[94m'
    end = '\033[0m'


####################################################################
#                          CONFIGURATIONS READER                   #
####################################################################
class MyConfigReader():
    '''
    Read FLEXO's configuration file and retrieve the necessary data
    '''

    DEFAULT_DICT = {
                    "Method": {"theory_level": "MM",
                               "explorer": "GA"
                               },
                    "Molecular Mechanics parameters": {"force_field": "MMFF94"
                                                      },
                    "Semi-Empirical parameters": {"se_method": "PM6",
                                                  "charge": 0
                                                  },
                    "Semi-Empirical programs parameters": {"name": "mopac",
                                                           "mopac_path": "",
                                                           "dftb_path": ""
                                                           },
                    "Limits": {"dihedral_minvalue": -180.,
                               "dihedral_maxvalue": 180.
                               },
                    "Genetic Algorithm parameters": {"members_number": 200,
                                                     "max_gen": 15
                                                     },
                    "BF parameters": {"angle_step": 60
                                      },
                    "PSO parameters": {"nparticles": 20,
                                       "pso_steps": 200
                                       },
                    "Misc. configurations" : {"initial_opt": True,
                                              "final_opt": False,
                                              "max_final_conformers": 10,
                                              "rmsd_threshold": 0.5,
                                              "energy_threshold": 2.0,
                                              "energy_limit": 2.5,
                                              "mask": "n:m",
                                              "range_generate": False,
                                              "save_all": False, "nprocs": 0
                                              },
                    "Output configurations": {"out_format": "mol2",
                                              "multimolecular_file": True,
                                              "report_file": "report.log"
                                              },
                    }

    def __init__(self):
        self.config_file = None
        self.report_file = None
        self.quiet = False

        self.configs = configparser.ConfigParser(inline_comment_prefixes="#")
        self.configs.read_dict(self.DEFAULT_DICT)

    def parse_command_line_arguments(self):
        '''
        Get the command's line arguments options.
        '''
        ########################################################################
        #                           ARGUMENTS PARSER                           #
        ########################################################################
        class ArgumentParser(argparse.ArgumentParser):
            def exit(self, status=None, message=None):
                if status and message is None:
                    sys.exit()
                elif message:
                    print(message)
                sys.exit(status)

            def error(self, message):
                if message and message.split(":")[0] == 'the following arguments are required':
                    message = "A .mol2 molecular structure file must be provided."
                self.exit(2, f"{Bcolors.red_fail}\nERROR!{Bcolors.end} {message}\n")


        parser = ArgumentParser(description="Flexo: Conformer generator.")

        parser.add_argument("molecule_file",
                            type=str,
                            help="Path to a molecular structure in .mol2 format.")

        parser.add_argument("-r", "--report_file",
                             dest="report_file",
                             type=str,
                             metavar="REPORT_FILE",
                             help="Name of the file where the final report will be written, with the molecule name as the prefix.")

        parser.add_argument("-c", "--config_file",
                           dest="config_file",
                           type=str,
                           default="configs.ini",
                           metavar="CONFIG_FILE",
                           help="Name of configuration file to set up the program.")

        parser.add_argument("-q", "--quiet",
                           action="store_true",
                           help="Quiet run. Doesn't show anything in the terminal.")

        parser_args = parser.parse_args()

        if not os.path.exists(parser_args.molecule_file) and not parser_args.molecule_file.split("/")[-1].endswith(".mol2"):
            raise parser.error(f"Molecule file not found: {parser_args.molecule_file} - {Bcolors.warning_yellow}Also, it must be a .mol2!{Bcolors.end}")

        if not parser_args.molecule_file.split("/")[-1].endswith(".mol2"):
            raise parser.error(f"Incorrect molecule file extension: {parser_args.molecule_file}{Bcolors.warning_yellow} - It must be .mol2!{Bcolors.end}")

        self.molecule_file = parser_args.molecule_file
        self.config_file = parser_args.config_file
        self.report_file = parser_args.report_file
        self.quiet = parser_args.quiet

    def read_configs(self, config_file):
        self.set_configs(config_file=config_file)

    def set_configs(self, config_file=None, config_dict=None):
        '''
        Read FLEXO's explorer configuration options from file o dict and set them for this run.

        Parameters
        ----------
        config_file: str
            Path to the configurations file. See :ref:`Configs`.
        config_dict: dict
            Optional dict to read configurations from instead of the configs file.

        Raises
        ------
        RuntimeError
            If no configurations were supplied.
        '''
        if config_file is None:
            if config_dict is None:
                print(f"No configurations file or dict has been pased!")
            else:
                self.configs.read_dict(config_dict)
        else:
            if not self.configs.read(config_file):
                raise ValueError(f"Configuration file \"{config_file}\" not found!")


        ##########
        # Method #
        ##########

        #Theory Level
        self.theory_level = self.configs.get("Method", "theory_level").upper()
        if self.theory_level not in ("MM", "OMM", "SE", "DFTB"):
            raise ValueError(f"{Bcolors.red_fail}Wrong theory level keyword: {self.theory_level}{Bcolors.end}")

        self.explorer = self.configs.get("Method", "explorer").upper()
        if self.explorer not in ["GA", "BF", "RBF", "PSO"]:
            raise ValueError(f"{Bcolors.red_fail}Wrong explorer keyword: {self.explorer}{Bcolors.end}")

        ##################################
        # Molecular Mechanics parameters #
        ##################################

        if self.theory_level in ("MM", "OMM"):
            #Force field
            self.force_field = self.configs.get("Molecular Mechanics parameters", "force_field", fallback="MMFF94")

        #############################
        # Semi-Empirical parameters #
        #############################

        elif self.theory_level == "SE":
            #Charge of the molecule
            self.charge = self.configs.getint("Semi-Empirical parameters", "charge", fallback=0)

            #SE Method
            self.se_method = self.configs.get("Semi-Empirical parameters", "se_method", fallback="PM6").upper()

            ######################################
            # Semi-Empirical programs parameters #
            ######################################

            default_se_executables =   {"MOPAC" : "/opt/mopac/MOPAC2016.exe",
                                        "DFTB"  : "/bin/dftb"}

            def get_executable(exepath, program):
                if exepath == "":
                    print(f"Falling to default executable for {program}: {default_se_executables[program]}")
                    exepath = default_se_executables[program]

                if os.path.isfile(exepath):
                    return exepath
                exename = exepath.split("/")[-1]
                for path in os.environ["PATH"].split(os.pathsep):
                    exepath = path + exename
                    if os.path.isfile(exepath):
                        print(f"Using executable found in path for {program}: {exepath}")
                        return exepath
                raise ValueError(f"{Bcolors.red_fail}Executable not found for {program}{Bcolors.end}")


            if self.configs.has_section("Semi-Empirical programs parameters"):
                self.se_program_name = self.configs.get("Semi-Empirical programs parameters", "name").upper()
                if not self.se_program_name in ["MOPAC", "DFTB", "ORCA"]:
                    raise ValueError(f"{Bcolors.red_fail}Wrong semi-empirical program keyword: {self.se_program_name}{Bcolors.end}")
                se_exe_path = self.configs.get("Semi-Empirical programs parameters", f"{self.se_program_name.lower()}_path", fallback="")
                self.se_exe_path = get_executable(se_exe_path, self.se_program_name)
            else:
                raise ValueError(f"{Bcolors.red_fail}Missing semi-empirical parameters.{Bcolors.end}")

        ##########
        # Limits #
        ##########

        #Maximum diheadral angle value
        self.dihedral_minvalue = self.configs.getfloat("Limits", "dihedral_minvalue", fallback=-180.)

        #Minimum diheadral angle value
        self.dihedral_maxvalue = self.configs.getfloat("Limits", "dihedral_maxvalue", fallback=180.)

        ################################
        # Genetic Algorithm parameters #
        ################################

        #Number of members
        self.members_number = self.configs.getint("Genetic Algorithm parameters", "members_number", fallback=200)

        #Maximum number of generations
        self.max_gen = self.configs.getint("Genetic Algorithm parameters", "max_gen", fallback=15)

        #################
        # BF parameters #
        #################

        #Angle step
        self.angle_step = self.configs.getint("BF parameters", "angle_step", fallback=120)

        #################
        # PSO parameters #
        #################

        #Number of particles
        self.nparticles = self.configs.getint("PSO parameters", "nparticles", fallback=20)
        self.pso_steps = self.configs.getint("PSO parameters", "pso_steps", fallback=200)

        ########################
        # Misc. configurations #
        ########################
        #initial optimization of the conformers
        self.initial_opt = self.configs.getboolean("Misc. configurations", "initial_opt", fallback=False)

        #Final optimization of the conformers
        self.final_opt = self.configs.getboolean("Misc. configurations", "final_opt", fallback=False)

        #Max number of final conformers.
        self.max_final_conformers = self.configs.getint("Misc. configurations", "max_final_conformers", fallback=20)

        self.rmsd_threshold = self.configs.getfloat("Misc. configurations","rmsd_threshold", fallback=0.5)
        self.energy_threshold = self.configs.getfloat("Misc. configurations", "energy_threshold", fallback=2.0)
        self.range_generate = self.configs.getboolean("Misc. configurations", "range_generate", fallback=False)
        self.energy_limit = self.configs.getfloat("Misc. configurations", "energy_limit", fallback=2.5)
        self.mask = self.configs.get("Misc. configurations", "mask", fallback="n:m")

        self.save_all = self.configs.getboolean("Misc. configurations", "save_all", fallback=False)

        if self.save_all:
            print("All confomers generated will be saved.")

        #Cores to use
        self.nprocs = self.configs.getint("Misc. configurations", "nprocs", fallback=0)
        if self.nprocs == 0:
            self.nprocs = os.cpu_count()

        #########################
        # Output configurations #
        #########################

        #Output format
        self.out_format = self.configs.get("Output configurations", "out_format", fallback="mol2")

        #Multimolecular file
        self.multimolecule_file = self.configs.getboolean("Output configurations", "multimolecule_file", fallback=True)

        #Report file
        if self.report_file is None:
            self.report_file = self.configs.get("Output configurations", "report_file", fallback="report.log")

    def write_configs(self, outfilelike=None):
        """Write the configurations to a file-like object, or else to configs.ini"""
        if outfilelike is None:
            with open("configs.ini", "w") as outfilelike:
                self.configs.write(outfilelike)
        else:
            self.configs.write(outfilelike)



def main(configs=None, molecule_file=None):
    '''
    Run the FLEXO program with the supplied configs and molecule.

    Parameters
    ----------
    configs : MyConfigReader
        Configurations to use. If None, an attempt is made to read the command line arguments.
    molecule_file : str
        Path to thr mol2 file defining the molecule and starting coordinates.

    '''
    ###############################
    # LOGO, FORMATTING AND COLORS #
    ###############################
    FLEXO_LOGO = r"""             ______   _
            |  ____| | |
            | |__    | |   ___  __  __   ___
            |  __|   | |  / _ \ \ \/ /  / _ \
            | |      | | |  __/  >  <  | (_) |
            |_|      |_|  \___| /_/\_\  \___/
    """
    LOGO_UNDERLINE = "        ----------------------------------\n"


    #### Read config file if not given
    if configs is None:
        configs = MyConfigReader()
        configs.parse_command_line_arguments()
        configs.read_configs(configs.config_file)

    if molecule_file is not None:
        configs.molecule_file = molecule_file

    ####################################################################
    #                              PROGRAM                             #
    ####################################################################
    #Initialize the molecule.
    molecule = objects.MyMolecule(configs.molecule_file)

    #Initialize the proper calculator.
    if configs.theory_level == "MM":
        calculator = MMCalculator(configs.force_field, molecule.file_path)
    ## Test keyword for new ob implementation
    elif configs.theory_level == "OMM":
        calculator = PybelCalculator(configs.force_field, molecule.file_path)

    elif configs.theory_level == "SE" and configs.se_program_name == "MOPAC":
        calculator = MopacCalculator(configs.se_method, configs.charge, configs.se_exe_path, mopac_extra_keywords=f"THREADS={configs.nprocs}")

    elif configs.theory_level == "SE" and configs.se_program_name == "DFTB":
        #If necesary, make the dftb_tmp folder.
        try:
            os.mkdir("dftb_tmp")
        except FileExistsError:
            pass
        calculator = DFTBCalculator(configs.se_exe_path)

    #Make an initial optimization and use this coordinates, before anything else
    if configs.initial_opt:
        print("Performing initial optimization of input coordinates.")
        try:
            os.mkdir(f"{molecule.name}_se_conformers_opt")
        except FileExistsError:
            print(f"The {molecule.name}_se_conformers_opt directory exists, to continue please delete it.")
            raise RuntimeError(f"The {molecule.name}_se_conformers_opt directory exists, to continue please delete it.")
        initial_opt_coords, initial_opt_e = calculator.get_opt(molecule.atoms_properties["symbol"],
                                                               molecule.initial_conformer.atoms_coords,
                                                               0,
                                                               molecule.name)
        molecule.initial_conformer.atoms_coords = initial_opt_coords
        molecule.initial_conformer.energy = initial_opt_e
        shutil.rmtree(f"{molecule.name}_se_conformers_opt")

    #Get type of parameter to modify.
    dihedrallst = mainfunctions.identify_dihedral(molecule, configs.mask)

    parameters_to_modify = []

    for dihedral in dihedrallst:
        value = mainfunctions.get_dihedral(molecule, dihedral)

        mask_frag = molecule.detect_fragments(dihedral)

        parameters_to_modify.append(objects.Parameter("D", dihedral, value, configs.dihedral_minvalue,
                                                      configs.dihedral_maxvalue, mask_frag))

    #Get initial state of parameters
    molecule.initial_paramlst = parameters_to_modify

    #Print relevant data.
    if not configs.quiet:
        print(FLEXO_LOGO, LOGO_UNDERLINE, sep="")
        print(f"Parameters to modify: {len(parameters_to_modify)}")
        if configs.theory_level == "MM":
            print(f"Forcefield: {configs.force_field}")
        elif configs.theory_level == "SE" and configs.se_program_name == "MOPAC":
            print(f"MOPAC's input line: {configs.se_method} CHARGE={configs.charge} 1SCF")
        if configs.save_all:
            print("All confomers generated will be saved.")

    for parameter in molecule.initial_paramlst:
        if not configs.quiet:
            if parameter.paramtype == "D":
                print(f"Atoms in dihedral angle: {parameter.atoms}. Initial value: {parameter.value:.2f}º")
            elif parameter.paramtype == "A":
                print(f"Atoms in angle: {parameter.atoms}. Initial value: {parameter.value:.2f}º")
            elif parameter.paramtype == "B":
                print(f"Atoms in bond: {parameter.atoms}. Initial value: {parameter.value:.2f}A")

    #Start time.
    start_datetime = datetime.datetime.now()
    if not configs.quiet:
        print(f"Starting date: {start_datetime.strftime('%Y-%m-%d %H:%M')}")


    explorers={"BF": bf, "GA":ga, "RBF": rbf, "PSO": pso}


    if len(parameters_to_modify) == 0:
        conformers_folder_path = f"{molecule.name}_{configs.theory_level.lower()}_conformers"
        print(f"{Bcolors.warning_yellow}\n{molecule.name} doesn't have parameters to modify!\n{Bcolors.end}"
            "Copying orignal file for convenience in post-scripting...\n ")
        molecule.initial_conformer.energy = calculator.get_energy(molecule.atoms_properties["symbol"],
                                                                  molecule.initial_conformer.atoms_coords,
                                                                  0,
                                                                  molecule.name)
        bestconformerlst, bestconf_energy, average_energy_bestconf = ([molecule.initial_conformer], molecule.initial_conformer.energy, molecule.initial_conformer.energy)

    else:
        with Pool(processes=configs.nprocs) as pool:
            #Set the explorator to be used.
            explorator = explorers[configs.explorer](molecule, calculator,
                                        max_gen = configs.max_gen,
                                        max_final_conformers = configs.max_final_conformers,
                                        energy_threshold = configs.energy_threshold,
                                        rmsd_threshold = configs.rmsd_threshold,
                                        range_generate = configs.range_generate,
                                        energy_limit = configs.energy_limit,
                                        members_number = configs.members_number,
                                        quiet = configs.quiet,
                                        pool = pool,
                                        dihedral_step = configs.angle_step,
                                        nparticles = configs.nparticles,
                                        pso_steps = configs.pso_steps,
                                        save_all = configs.save_all
                                        )
            bestconformerlst, bestconf_energy, average_energy_bestconf = explorator.get_conformers()

    ##Write the files containing the new conformers.
    conformers_folder_path = f"{molecule.name}_{configs.theory_level.lower()}_{configs.explorer.lower()}_conformers"
    try:
        os.mkdir(conformers_folder_path)
    except FileExistsError:
        pass
    os.chdir(conformers_folder_path)

    if configs.multimolecule_file is True:
        #Create and move to temporal dir for multimolecular creation
        multitempdir = "multimolecular_tmp"
        try:
            os.mkdir(multitempdir)
        except FileExistsError:
            pass
        os.chdir(multitempdir)

    for i,_ in enumerate(bestconformerlst):
        if configs.out_format == 'mol2':
            mainfunctions.conformer_to_mol2(bestconformerlst[i], molecule, f"{molecule.name}_conformer{i}.mol2")
        elif configs.out_format == 'xyz':
            mainfunctions.conformer_to_xyz(bestconformerlst[i], molecule, f"{molecule.name}_conformer{i}.xyz")

    if configs.multimolecule_file is True:
        # Sort by the conformer number , not by file name
        prefix_len=len(f"{molecule.name}_conformer")
        parts = sorted(os.listdir(), key=lambda filename: int(filename[prefix_len:].split(".")[0]))
        # Write output out of temp dir and delete it
        with open(f"../{molecule.name}_conformers.{configs.out_format}", "w") as multif:
            for part_files in parts:
                with open(part_files) as part:
                    multif.write(part.read())
                os.remove(part_files)
        os.chdir("../")
        shutil.rmtree(multitempdir)
    #############################################
    # FINAL GEOMETRY OPTIMIZATION RELATED BLOCK #
    #############################################
    if configs.final_opt is True:
        opt_conformers_folder_path = f"{molecule.name}_{configs.theory_level.lower()}_conformers_opt"
        if calculator.__class__.__name__ == "MopacCalculator":
             #If necesary, make the molecule_name_opt folder.
            try:
                os.mkdir(opt_conformers_folder_path)
            except FileExistsError:
                pass
            if not configs.quiet:
                print (f"{Bcolors.running_blue}\nMOPAC is running the final geometry optimizations...{Bcolors.end}")
            with Pool() as pool:
                opt_conformers = mainfunctions.optimize_geom(bestconformerlst,
                                                            molecule,
                                                            calculator,
                                                            pool=pool,
                                                            out_format=configs.out_format,
                                                            range_generate=configs.range_generate,
                                                            max_final_conformers=configs.max_final_conformers,
                                                            energy_threshold=configs.energy_threshold,
                                                            rmsd_threshold=configs.rmsd_threshold
                                                            )

        elif calculator.__class__.__name__ in ["PybelCalculator", "MMCalculator"]:
            #If necesary, make the molecule_name_opt folder.
            try:
                os.mkdir(opt_conformers_folder_path)
            except FileExistsError:
                pass #This is dangerous. We should change it ASAP.
            if not configs.quiet:
                print (f"{Bcolors.running_blue}\nRunning Open Babel final geometry optimization...{Bcolors.end}")
            with Pool() as pool:
                opt_conformers = mainfunctions.optimize_geom(bestconformerlst,
                                                            molecule,
                                                            calculator,
                                                            pool=pool,
                                                            out_format=configs.out_format,
                                                            range_generate=configs.range_generate,
                                                            max_final_conformers=configs.max_final_conformers,
                                                            energy_threshold=configs.energy_threshold,
                                                            rmsd_threshold=configs.rmsd_threshold
                                                            )
        elif calculator.__class__.__name__ == "DFTBCalculator":
            print(f"{Bcolors.warning_yellow}Final optimization using DFTB is not yet implemented in flexo."
                  f"Please do the optimizations outside this program.{Bcolors.end}")
        try:
            os.mkdir(opt_conformers_folder_path)
        except FileExistsError:
            pass
        os.chdir(opt_conformers_folder_path)
        if configs.multimolecule_file is True:
        #Create and move to temporal dir for multimolecular creation
            multitempdir = "multimolecular_tmp"
            try:
                os.mkdir(multitempdir)
            except FileExistsError:
                pass
            os.chdir(multitempdir)

        for i,_ in enumerate(opt_conformers):
            if configs.out_format == 'mol2':
                mainfunctions.conformer_to_mol2(opt_conformers[i], molecule, f"{molecule.name}_conformer{i}_opt.mol2")
            elif configs.out_format == 'xyz':
                mainfunctions.conformer_to_xyz(opt_conformers[i], molecule, f"{molecule.name}_conformer{i}_opt.xyz")
        if configs.multimolecule_file is True:
            # Sort by the conformer number , not by file name
            prefix_len = len(f"{molecule.name}_conformer")
            parts = sorted(os.listdir(), key=lambda filename: int(filename[prefix_len:].split("_")[0]))
            # Write output out of temp dir and delete it
            with open(f"../{molecule.name}_conformers_opt.{configs.out_format}", "w") as multif:
                for part_files in parts:
                    with open(part_files) as part:
                        multif.write(part.read())
                    os.remove(part_files)
            os.chdir("../")
            shutil.rmtree(multitempdir)
        #Back to top dir for cleanup
        os.chdir("../")
    #Report file.
    parameters = {"GA": ("dihedral_minvalue", "dihedral_maxvalue", "members_number", "max_gen", "max_final_conformers"),
                  "BF": ("angle_step",)}

    with open(f"{molecule.name}_{configs.report_file}", "w") as outfile:
        print("#Configs#", file=outfile)
        print(f"Explorer: {configs.explorer}", file=outfile)
        if configs.theory_level in ("MM", "OMM"):
                  print(f"theory_level: {configs.theory_level}\nforce_field:{configs.force_field}", file=outfile)
        elif configs.theory_level == "SE":
                  print(f"theory_level: {configs.theory_level}\nmethod:{configs.se_method}", file=outfile)
        for parameter in parameters.get(configs.explorer, []):
            print(f"{parameter}: {getattr(configs, parameter)}", file=outfile)
        print(f"\nfinal_opt: {configs.final_opt}", end='\n', file=outfile)
        end_datetime = datetime.datetime.now()
        total_datetime = end_datetime - start_datetime
        total_hours, rest = divmod(total_datetime.total_seconds(), 3600)
        total_minutes, total_seconds = divmod(rest, 60)
        print("\n#Results#"
              f"\nStart date: {start_datetime.strftime('%Y-%m-%d %H:%M')}"
              f"\nEnd date: {end_datetime.strftime('%Y-%m-%d %H:%M')}"
              f"\nTotal time: {int(total_hours)}:{int(total_minutes)}:"
              f"{total_seconds}"
              f"\nBest conformer energy: {bestconf_energy:.4f} kcal/mol."
              f"\nAverage energy of the final assembly: {average_energy_bestconf:.4f} kcal/mol."
            , file=outfile)
        print("\n#Dihedrals#", file=outfile)
        for dihedral in dihedrallst:
            print(f"{dihedral}", file=outfile)
    os.chdir("../")
    ############
    # CLEAN UP #
    ############

    try:
        shutil.rmtree("dftb_tmp")
    except FileNotFoundError:
        pass

    if not configs.quiet:
        print(f"{Bcolors.green_ok}\nEverything finished OK!\n  QVOD DIES PERDIDIT  \n{Bcolors.end}")
    


if __name__ == "__main__":
    main()
