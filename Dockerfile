FROM continuumio/miniconda3

USER 0

# Import conda enviroment file
# ADD environment.yml /tmp/environment.yml
# RUN conda env create -f /tmp/environment.yml
RUN conda create -n flexoenv python==3.12

# Pull the environment name out of the environment.yml
RUN echo "source activate flexoenv" > ~/.bashrc
ENV PATH=/opt/conda/envs/flexoenv/bin:$PATH

RUN apt update && apt install -y less vim
RUN apt install -y openbabel libopenbabel-dev
RUN pip install build 
# Set work directory
WORKDIR /flexo

# Copy project
ADD . /flexo

RUN pip install numpy==1.26.4 pybel scipy
RUN python -m build -o /dist

ENTRYPOINT ["bash"]
