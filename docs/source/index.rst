.. FLEXO documentation master file, created by
   sphinx-quickstart on Wed Sep 22 09:51:40 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to FLEXO's documentation!
=================================

.. note::
    This project in under active and chaotic development.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. toctree::
    :maxdepth: 1
    :caption: Contents:
    
    about/FAQ
    tutorials/getting-started
    about/configs
    modules/flexo
    modules/objects
    modules/calculators
    modules/explorators
    modules/mainfunctions
    modules/cfunctions
    about/specific-options


