.. _configs:

Configurations file
===================

The FLEXO configurations are read fom the ``configs.ini`` file. The configs syntax follows that from `the configparser documentation <https://docs.python.org/3/library/configparser.html#supported-ini-file-structure>`_ - after all, that is the module used to read the configurations file.

In a nutshell, the file is divided into sections where specific configurations are grouped. The sections are defined by a section name enclosed in square brackets. Then, the next lines contain a space separated configuration and value pair.

As an demostrative example:

.. literalinclude:: ../tutorials/configs_example.ini
    :language: INI

Now, each available section with its configurations and acceptable values are detailed.

\[Method\]
----------

+-----------------+------------+--------------------+------------------------------------------------------+
| Config          | Default    | Acceptable values  | Description                                          |
+=================+============+====================+======================================================+
|theory_level     |  MM        | MM, SE             | Theory level to use. See :ref:`calculators`          |
+-----------------+------------+--------------------+------------------------------------------------------+
|explorer         |  GA        + GA, PSO, RBF, BF   | GA) Genetic Algorithm or (BF) Brute Force.           |
+-----------------+------------+--------------------+------------------------------------------------------+

\[Molecular Mechanics parameters\]
----------------------------------

+-----------------+------------+--------------------+------------------------------------------------------+
| Config          | Default    | Acceptable values  | Description                                          |
+=================+============+====================+======================================================+
|force_field      |  MMFF94    | MMFF94, GAFF, UFF  | Force field to use with the MM calculators.          |
+-----------------+------------+--------------------+------------------------------------------------------+

\[Semi-Empirical parameters\]
-----------------------------

+-----------------+------------+--------------------+------------------------------------------------------+
| Config          | Default    | Acceptable values  | Description                                          |
+=================+============+====================+======================================================+
|se_method        | PM6        | PM6, AM1, etc...   | Semi-Emporirical hamiltonian to use.                 |
+-----------------+------------+--------------------+------------------------------------------------------+
|charge           | 0          | integer values     | Net charge of the molecule.                          |
+-----------------+------------+--------------------+------------------------------------------------------+

\[Semi-Empirical programs parameters\]
--------------------------------------

+-----------------+------------+--------------------+------------------------------------------------------+
| Config          | Default    | Acceptable values  | Description                                          |
+=================+============+====================+======================================================+
| name            | mopac      | mopac, dftb        | Name of the program to use for SE calculations.      |
+-----------------+------------+--------------------+------------------------------------------------------+
| mopac_path      |            | Path-like str      | Path to the mopac executable.                        |
+-----------------+------------+--------------------+------------------------------------------------------+
| dftb_path       |            | Path-like str      | Path to the dftb executable.                         |
+-----------------+------------+--------------------+------------------------------------------------------+

\[Limits\]
----------

+-------------------+------------+--------------------+------------------------------------------------------+
| Config            | Default    | Acceptable values  | Description                                          |
+===================+============+====================+======================================================+
| dihedral_minvalue | -180       | int \[-180, 180\]  | Lower limit to explored dihedrals values.            |
+-------------------+------------+--------------------+------------------------------------------------------+
| dihedral_maxvalue |  180       | int \[-180, 180\]  | Upper limit to explored dihedrals values.            |
+-------------------+------------+--------------------+------------------------------------------------------+

\[Genetic Algorithm parameters\]
--------------------------------

+-------------------+------------+--------------------+------------------------------------------------------+
| config            | default    | acceptable values  | description                                          |
+===================+============+====================+======================================================+
| members_number    | 200        | positive int       | Number of members generated per ga cycle.            |
+-------------------+------------+--------------------+------------------------------------------------------+
| max_gen           | 15         | positive int       | Maximum number of generations of the ga.             |
+-------------------+------------+--------------------+------------------------------------------------------+


\[BF parameters\]
-----------------

+------------+---------+-------------------+---------------------------------------------------+
| config     | default | acceptable values | description                                       |
+============+=========+===================+===================================================+
| angle_step | 60      | 360 divisors      | Step to use for the Brute Force grid exploration. |
+------------+---------+-------------------+---------------------------------------------------+

\[PSO parameters\]
------------------

+------------+---------+-------------------+------------------------------------------+
| config     | default | acceptable values | description                              |
+============+=========+===================+==========================================+
| nparticles | 20      | Positive Integers | Number of particles to use in the swarm. |
+------------+---------+-------------------+------------------------------------------+
| pso_steps  | 200     | Positive Integers | Number of steps of the PSO algorithm.    |
+------------+---------+-------------------+------------------------------------------+

\[Misc. configurations\]
------------------------

+----------------------+---------+-------------------+-----------------------------------------------------------------------+
| config               | default | acceptable values | description                                                           |
+======================+=========+===================+=======================================================================+
| mask                 | m:n     | m:n, int:int      | Defines atoms to move. Default is all. See :ref:`mask`                |
+----------------------+---------+-------------------+-----------------------------------------------------------------------+
| initial_opt          | False   | True or False     | Wether or not to optimize the input coordinates.                      |
+----------------------+---------+-------------------+-----------------------------------------------------------------------+
| final_opt            | False   | True or False     | Wether or not to optimize the input coordinates.                      |
+----------------------+---------+-------------------+-----------------------------------------------------------------------+
| max_final_conformers | 20      | Positive integer  | Return at most this amount of conformers.                             |
+----------------------+---------+-------------------+-----------------------------------------------------------------------+
| rmsd_threshold       | 0.5     | Positive values   | RMSD used to consider two conformers as different.                    |
+----------------------+---------+-------------------+-----------------------------------------------------------------------+
| energy_threshold     | 2.0     | Positive values   | :math:`\Delta` E to consider two confs as different                   |
+----------------------+---------+-------------------+-----------------------------------------------------------------------+
| range_generate       | False   | Boolean           | Generate conformers in an energy range. See :ref:`range_generate`.    |
+----------------------+---------+-------------------+-----------------------------------------------------------------------+
| energy_limit         | 2.5     | Positive values   | Energy used in range_generate. See :ref:`range_generate`.             |
+----------------------+---------+-------------------+-----------------------------------------------------------------------+
| save_all             | False   | Boolean           | Whether or not to save all generated conformers. See :ref:`save_all`. |
+----------------------+---------+-------------------+-----------------------------------------------------------------------+
| nprocs               | 0       | Positive integer  | Number of cores to use. All by default. **Not yet implemented**.      |
+----------------------+---------+-------------------+-----------------------------------------------------------------------+

\[Output configurations\]
-------------------------

+---------------------+------------+-------------------+--------------------------------------------------------------------------------------------------------+
| config              | default    | acceptable values | description                                                                                            |
+=====================+============+===================+========================================================================================================+
| out_format          | mol2       | mol2, xyz         | Molecular format of the output structures.                                                             |
+---------------------+------------+-------------------+--------------------------------------------------------------------------------------------------------+
| multimolecular_file | True       | Boolean           | If true, all conformers will be written to the same multimolecular file. Else, one conformer per file. |
+---------------------+------------+-------------------+--------------------------------------------------------------------------------------------------------+
| report_file         | report.log | File name strings | Name of the file where the run info will be written.                                                   |
+---------------------+------------+-------------------+--------------------------------------------------------------------------------------------------------+
