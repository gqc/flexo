FAQs
====

.. note::
    This project is fairly new, so the FAQs are not *really* frequently asked, it's what we imagine they may be (? If you have any questions `contact us <https://gqc.quimica.unlp.edu.ar/>`_.

- What is FLEXO?

Flexo is a conformer searching tool.

- How does FLEXO work?

Flexo generates conformers by exploring the dihedral conformational space of a given molecule. That is, the molecule's dihedrals are rotated, generating new conformers, but bonds and angles are not modified. The algorithms that generate conformers are called explorators. Included explorators are Genetic Algorithm (GA), Particle Swarm Optimization (PSO) and Brute force (BF).

- So bonds and angles are not modified?

Inside the algorithm they are not. However, flexo provides the option to optimize the initial coordinates, so that these parameters are relaxed before the dihedrals are modified.

- What level of theory does FLEXO use?

The explorators need to evaluate each generated conformer's energy. To this end there are calculators. Included calculators use Molecular Mechanics (MM) or Semi-Empirical (SE) methods. Available MM forcefields are MMFF94 and GAFF. More functionalities are available on the open source downloadable version. 