.. _save_all:

Save all
========

Save all is a feature included in all default explorators, which writes out all the generated conformers in the exploration. The output file is named ``out${exp}file_${molecule}.log``, where exp references the used explorer, and molecule is the molecule's name. For the diose molecule with tha GA explorator, the file would be ``outgafile_diose.log``

Per line, the file contains a secuence with the values of the molecule's dihedrals, then the conformer's energy, and finally an integer classifying the conformer. The classification is algorithmic dependent. For example, for GA the integer indicates the generation where the conformer was generated. For BF, the index is not relevant, and for PSO it represents the particle that generated the conformer.

The file is easily parseable to obtain info, such as the PSO Particle's trajectories.

The configuration is set from the ``[Misc. configurations]`` section, `save_all` option in the :ref:`configs.ini <Configs>` file.