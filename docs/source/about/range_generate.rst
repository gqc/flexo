.. _range_generate:

Range generate
==============

The range generate feature generates conformers with energy :math:`E` so that :math:`|E - E_{min}| < \Delta E_{RG}`, donde:

- :math:`E_{min}` is the energy of the lowest-energy conformer.
- :math:`\Delta E_{RG}` is setted with the range_generate options in ``configs.ini``.