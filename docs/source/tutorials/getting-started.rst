Getting started
===============

.. _https://gqc.quimica.unlp.edu.ar/flexo: https://gqc.quimica.unlp.edu.ar/flexo

To start using FLEXO you need at least 3 ingredients: your molecule of interest in ``mol2`` format, and to choose a :ref:`calculator <Calculators>` (level of theory) and :ref:`explorator <Explorators>`.

You can use FLEXO from the command line, as a python module or from its web server, over at `https://gqc.quimica.unlp.edu.ar/flexo`_.

Web server
------------

Its the most straightforward way to use FLEXO - but also the most restricted. You just need to go to `https://gqc.quimica.unlp.edu.ar/flexo`_,  create an account, and you're all set. In the Submission page you can submit a molecule - either by drawing it in the molecule editor or writing the SMILES. 

Several molecules can be submitted at a time by uploading a plain text file with one SMILES per line. Additionaly, a "name" extra column can be supplied.

By restricted I mean you can only have at most 50 molecules queued at any time, and the available configurations are minimal. Nevertheless, it still works. More info is in the webpage's help page.

Command Line
------------

An example command from the CLI would be

.. code:: 

    python3 flexo.py ../example_molecules/diose.mol2 -c configs.ini

with this command, FLEXO generates conformers for the `diose <https://pubchem.ncbi.nlm.nih.gov/compound/756>`_ molecule supplied as a mol2 file, with the configurations read from the configs.ini file.

After the run, you will see a new folder named ``diose_mm_ga_conformers``. In this folder, you will find the report.log file, which has a rundown of the calculation, and the diose_conformers.mol2 file, where the conformers in mol2 format are found.

The configs.ini file determines the executed calculation. An example configuration file would be:

.. literalinclude:: configs_example.ini
    :language: INI

A complete description of the options is at the :ref:`Configs`. In each line of this example, we tell FLEXO to:

- Use Molecular Mechanics methods.
- Use a Genetic Algorithm for conformer generation.
- Use the MMFF94 force field for energy evaluation.
- Perform a optimization of the initial coodinates before confomer generation.
- Not to optimize the generated conformers.
- Get at most 10 output conformers.
- Explore dihedrals in the [-180, 180] range (all possible range).

And this produces the output:

.. literalinclude:: example_output.log

The output shows info of the run and the algorithm used. It starts with a nice logo and details of the run, and then algorithm specific info. For GA, that is average conformer energy, best conformer energy and time per cycle. Worth mentioning is just below the logo, the atoms and angles comprising each explored dihedral.

By default, the conformers are written in ``mol2`` format, but you can algo get them in ``xyz`` format (:ref:`Configs`).

FLEXO as a module
-----------------

Importing the flexo module, you can access to all the functions and objects that make FLEXO. This includes distributed :ref:`calculators <Calculators>`, :ref:`explorators <Explorators>` and :ref:`utility functions <Mainfunctions>` to use on your own scripts. 

For example, you can access single point calculations and coordinate optimizations. Also, you can also use the :ref:`main <Main>` function to execute FLEXO as part of a script.
